# RNDF gazebo plugin

** Gazebo plugin for RNDF visualization **

  [http://bitbucket.org/JChoclin/rndf_gazebo_plugin](http://bitbucket.org/JChoclin/rndf_gazebo_plugin)

## Continuous integration

Please refer to the [Bitbucket Pipelines](https://bitbucket.org/JChoclin/rndf_gazebo_plugin/addon/pipelines/home#!/).

## Dependencies

The following dependencies are required to compile manifold from source:

 - cmake
 - git
 - cppcheck
 - C++ compiler with c++11 support (eg. GCC>=4.8).
 - Gazebo on this [commit](https://bitbucket.org/osrf/gazebo/commits/2b49dbedff87910898d507c09135e1f078c40f59)
 - Docker. You can work with [this](https://github.com/ekumenlabs/terminus/tree/master/docker) docker image and check this [issue](https://github.com/ekumenlabs/terminus/issues/77)

1. Install the build dependencies:

    ```
    sudo apt-get install build-essential cmake git cppcheck
    ```

    ```
    curl -ssL http://get.gazebosim.org | sh
    ```

Then, you can run Gazebo to check your installation:

    ```
    gazebo
    ```

Link the pre-commit hook:

    ```
    ln tools/pre-commit.sh .git/hooks/pre-commit
    ```
    
## Installation

Standard installation can be performed in UNIX systems using the following steps (or in the Docker container):

 - mkdir build/
 - cd build/
 - cmake -DCMAKE_PREFIX_PATH=/tmp/manifold/build/ ..
 - make

## Run on the docker container or in your UNIX system:

    ```
    cd ~/ws/rndf_gazebo_plugin
    source setup.bash
    gazebo --verbose <world_file>
    ```
Use:

 * --verbose: prints internal Gazebo messages and the plugin messages too.

 You can use one of the examples the repository already has in the folder examples.

### World file plugin description

 Your world file must have the following node inside the world node:

    ```
    <plugin name='rndf_gazebo_plugin0' filename='librndf_gazebo_plugin0.so.0.0.1'>
      <rndf>darpa.rndf</rndf>
      <lanes>true</lanes>
      <waypoints>true</waypoints>
      <junctions>true</junctions>
      <perimeter>true</perimeter>
      <waypoints_material>Gazebo/White</waypoints_material>
      <lane_material>Gazebo/Black</lane_material>
      <junction_material>Gazebo/Residential</junction_material>
      <perimeter_material>Gazebo/Blue</perimeter_material>
      <interpolation_distance>10.0</interpolation_distance>
      <origin>38.875413 -77.205045 12.3</origin>
      <print_labels>true</print_labels>
    </plugin>
    ```

* The tag `rndf` is mandatory. It includes the file path of the RNDF file.
* The `lanes` and `waypoint` tags are optional and its content should be true or false indicating if you want to print the lanes and or the waypoints.
* `junctions` also is optional and enable rendering the junctions.
* `perimeter` also is optional and enable rendering the perimeters.
* `waypoints_material` change the material applied to the waypoints.
* `print_labels` is optional, and lets you enable labels over the lanes
and the waypoints to display their name.
* `lane_material` change the material applied to the lanes.
* `junction_material` change the material applied to the junctions.
* `perimeter_material` change the material applied to the perimeters.
* `origin` is option and is made by a tridimensional double vector. The first value is the latitude (*38.875413*), the second one (*-77.205045*) is latitude and the last one is elevation (*12.3*). Both latitude and longitude must be expressed in degrees, and elevation in meters.


## Tests

This project includes GTests. To compile them, on the build directory after running cmake:

    ```
    make -j4
    make test
    ```

And you should see the test results

