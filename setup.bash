#!/bin/bash

source /usr/share/gazebo-8/setup.sh
export RNDF_PLUGIN_PATH=/home/gazebo/ws/rndf_gazebo_plugin
export TERMINUS_PATH=/home/gazebo/ws/src
export GAZEBO_RESOURCE_PATH=${RNDF_PLUGIN_PATH}/example:${GAZEBO_RESOURCE_PATH}
export GAZEBO_RESOURCE_PATH=${TERMINUS_PATH}:${GAZEBO_RESOURCE_PATH}
export GAZEBO_PLUGIN_PATH=${GAZEBO_PLUGIN_PATH}:${RNDF_PLUGIN_PATH}/build/src/rndf_gazebo_plugin/:${RNDF_PLUGIN_PATH}/build/src/road_driver_plugin/
export GAZEBO_MODEL_PATH=${GAZEBO_MODEL_PATH}:${RNDF_PLUGIN_PATH}/models
