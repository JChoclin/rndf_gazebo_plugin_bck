/*
 * Copyright (C) 2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/
#ifndef JUNCTION_CREATOR_HH
#define JUNCTION_CREATOR_HH

#include <gazebo/gazebo.hh>
#include <gazebo/util/system.hh>
#include <gazebo/msgs/msgs.hh>
#include <gazebo/rendering/rendering.hh>
#include <gazebo/rendering/RenderTypes.hh>
#include <gazebo/transport/TransportTypes.hh>
#include <gazebo/transport/Publisher.hh>

#include <ignition/math.hh>
#include <ignition/msgs.hh>
#include <ignition/transport.hh>

#include <manifold/rndf/RNDF.hh>
#include <manifold/rndf/Segment.hh>
#include <manifold/rndf/Lane.hh>
#include <manifold/rndf/Waypoint.hh>
#include <manifold/rndf/Exit.hh>
#include <manifold/rndf/UniqueId.hh>

#include <iostream>
#include <string>
#include <vector>
#include <functional>

#include "rndf_plugin_helpers.hh"
#include "math_util.hh"
#include "creator.hh"

namespace gazebo {

/// \brief It manages the waypoint, its pose and the extent points
/// of the lane
class RNDF_PLUGIN_VISIBLE LaneTermination {
  public:
    /// \brief Default constructor
    LaneTermination();
    /// \brief Constructor that handles all the instance variables
    /// \param[in] id The unique id of the waypoint.
    /// \param[in] pose The pose of the waypoint
    /// \param[in] laneWidth The width of the lane
    LaneTermination(const manifold::rndf::UniqueId &id,
      const ignition::math::Pose3d &pose,
      const double laneWidth);
    /// \brief Destructor
    ~LaneTermination();
    /// \brief It gives a reference to the id
    /// \return A reference to the uniqueId
    manifold::rndf::UniqueId& Id();
    /// \brief It gives a reference to the pose
    /// \return A reference to the pose
    ignition::math::Pose3d& Pose();
    /// \brief It gives a reference to the width of the lane
    /// \return A reference to the width
    double& LaneWidth();
    /// \brief A vector that has the extent points of the lane
    /// \return A vector with the extent points positions
    std::vector<ignition::math::Vector3d> ExtentPositions();
    /// \brief It manages the comparison by id of the
    /// lane termination
    struct IdMatch :
      public std::unary_function<manifold::rndf::UniqueId, bool>{
        /// \brief Constructor
        /// \param[in] id The id to compare to
        explicit IdMatch(manifold::rndf::UniqueId &_id) : id(_id) {}
        /// \brief The id to make the comparisson
        manifold::rndf::UniqueId id;
        /// \brief The function operator to call to validate
        /// the comparison
        /// \param[in] _id The value of the UniqueId to compare
        /// against id
        /// \return A boolean indcating true if it matches or false
        /// if not
        bool operator()(const manifold::rndf::UniqueId &_id) {
          if (id == _id)
            return true;
          return false;
        }
    };

  private:
    /// \brief The id of the waypoint
    manifold::rndf::UniqueId id;
    /// \brief The pose of the waypoint
    ignition::math::Pose3d pose;
    /// \brief The width of the lane where the waypoint is
    double laneWidth;
};

/// \brief This class handles all the junction lane terminations
/// and computes the points in such order that the visualization
/// is nice.
class RNDF_PLUGIN_VISIBLE Junction {
  public:
    /// \brief Default constructor
    Junction();
    /// \brief Constructor
    /// \param[in] laneTermination It gives a reference to the
    /// laneTerminations vector
    Junction(
      std::vector<gazebo::LaneTermination>& laneTerminations);
    /// \brief Destructor
    ~Junction();
    /// \brief Accessor to the LaneTermination vector
    /// \return A reference to the LaneTermination vector
    std::vector<gazebo::LaneTermination>& LaneTerminations();
    /// \brief It computes all the items of teh LaneTermination
    /// points and then it orders them in a polar way so the
    /// visualizaton works.
    void Compute();
    /// \brief Accessor of the pose of the junction. It is the
    /// center.
    /// \return A reference to the pose
    ignition::math::Pose3d& Pose();
    /// \brief Accessor for the points of the junctions
    /// \return A reference to a vector which contains the value
    /// of the points
    std::vector<ignition::math::Vector3d>& Points();
    /// \brief It checks if any of the lane terminations contains
    /// at least one of UniqueIds passed
    /// \param[in] ids A vector of UniqueIds to match
    /// \return True if any of the ids is matched or false if not
    bool ContainsAny(std::vector<manifold::rndf::UniqueId> &ids);
    /// \brief It finds the junction that matches by ids
    /// \param[in] junctions A vector refence of junctions
    /// \param[in] lts A vector reference of lane terminations
    /// \return The id of the junction inside the vector or -1 if
    /// it is not found
    static int GetJunctionIndexById(
      std::vector<gazebo::Junction> &junctions,
      std::vector<gazebo::LaneTermination> &lts);

  private:
    /// \brief It prints the points positions of the junction
    void PrintPoints();
    /// \brief A vector of laneTerminations
    std::vector<gazebo::LaneTermination> laneTerminations;
    /// \brief A vector of points that contains all the extents
    /// of the lane terminations
    std::vector<ignition::math::Vector3d> points;
    /// \brief The pose of the center of the junction.
    ignition::math::Pose3d pose;
};

/// \brief This is an utility class that handles the marker
/// message creation to create a junction.
class RNDF_PLUGIN_VISIBLE JunctionCreator : public Creator {
  public:
    /// \brief Constructor
    JunctionCreator();
    /// \brief Destructor
    ~JunctionCreator();
    /// \brief Setter for the junction reference
    /// \param[in] junction The junction to draw.
    void Junction(const gazebo::Junction &junction);
    /// \brief Setter for the material reference
    /// \param[in] material A string representation of the material
    void Material(const std::string &material);
    /// \brief It creates a convex polygon given the a set of
    /// verteces. It works with concave polygons too, but the
    /// baricenter of the polygon must be inside the perimeter.
    /// \return True if the message is sent, and false if it fails.
    bool Create();

  private:
    /// \brief The reference to the junction
    gazebo::Junction junction;
    /// \brief The reference to the material
    std::string material;
};

}

#endif
