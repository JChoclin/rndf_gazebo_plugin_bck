/*
 * Copyright (C) 2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/
#ifndef DYNAMIC_RENDER_HH
#define DYNAMIC_RENDER_HH

#include <gazebo/gazebo.hh>
#include <gazebo/util/system.hh>
#include <gazebo/msgs/msgs.hh>
#include <gazebo/rendering/rendering.hh>
#include <gazebo/rendering/RenderTypes.hh>
#include <gazebo/transport/TransportTypes.hh>
#include <gazebo/transport/Publisher.hh>
#include <gazebo/gui/GuiIface.hh>
#include <gazebo/gui/EntityMaker.hh>
#include <gazebo/gui/GuiEvents.hh>
#include <gazebo/math/Vector3.hh>
#include <gazebo/common/common.hh>
#include <gazebo/common/Console.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/rendering/UserCamera.hh>
#include <gazebo/math/Quaternion.hh>
#include <gazebo/common/MouseEvent.hh>
#include <gazebo/common/Exception.hh>
#include <sdf/Element.hh>
#include <ignition/math.hh>
#include <ignition/msgs.hh>
#include <ignition/transport.hh>


#include <manifold/rndf/RNDF.hh>
#include <manifold/rndf/Segment.hh>
#include <manifold/rndf/Lane.hh>
#include <manifold/rndf/Waypoint.hh>
#include <manifold/rndf/Exit.hh>
#include <manifold/rndf/UniqueId.hh>
#include <manifold/rndf/Zone.hh>
#include <manifold/rndf/Perimeter.hh>
#include <manifold/rndf/ParkingSpot.hh>
#include <manifold/rndf/RNDFNode.hh>
#include <manifold/RoadNetwork.hh>

#include <fstream>
#include <streambuf>
#include <sstream>
#include <chrono>
#include <thread>
#include <string>
#include <vector>
#include <algorithm>

#include "rndf_plugin_helpers.hh"
#include "road_creator.hh"
#include "waypoint_creator.hh"
#include "camera_controller.hh"
#include "projection.hh"
#include "perimeter_creator.hh"
#include "junction_creator.hh"

#include "./waypoints_request.pb.h"
#include "./waypoints_response.pb.h"

#define DEBUG_MSGS
#ifdef DEBUG_MSGS
  #define DEBUG_MSG(str) do { std::cout << str << std::endl; } while (false)
#else
  #define DEBUG_MSG(str) do { } while (false)
#endif

namespace gazebo {

class RNDF_PLUGIN_VISIBLE DynamicRender{
  typedef const boost::shared_ptr<
    const rndf_gazebo_plugin_msgs::msgs::WaypointsRequest> WaypointsRequestPtr;

  public:

    /// \brief Default constructor.
    DynamicRender();
    /// \brief Destructor.
    virtual ~DynamicRender();
    /// \brief It loads the Update event handler and tries to load the
    ///  RNDF file.
    void Load(gazebo::physics::WorldPtr _parent, sdf::ElementPtr _sdf);
    /// \brief This method is the callback handler of the topic that listens
    /// to incomming messages from the road driver pluglin.
    /// \details It receives a waypoint Id and then it loads different vectors,
    /// such as the control points positions, the road waypoint ids, the entry
    /// and exits.
    /// \param[in] wpId a pointer to a message that describes the uniqueId of
    /// the vertex in the graph
    void OnPublishWaypoints(const manifold::rndf::UniqueId &wpId);
    /// \brief Answer Waypoint Request Messages publishing a Waypoint Response
    // message with the lane waypoints positions, entries ids and entries
    // positions
    // \param _msg message received
    void OnWaypointsRequestsMsgs(WaypointsRequestPtr &_msg);

  private:

    /// \brief It loads a sample RNDF file using Manifold Library
    /// \throws gazebo::common::Exception If the RNDF file isn't successfully
    /// loaded
    void LoadRNDFFile();
    /// \brief It prints the RNDF file stats using Manifold Library
    void PrintRNDFStats();
    /// \brief It loads the segments in Gazebo
    /// \param[in] segments It is the vector of segments to draw
    void LoadSegments(std::vector<manifold::rndf::Segment> &segments);
    /// \brief It loads the lanes in Gazebo
    /// \param[in] segmentParent It is the parent segment id
    /// \param[in] lanes It is the vector of lanes to draw
    void LoadLanes(const int segmentParent,
      std::vector<manifold::rndf::Lane> &lanes);
    /// \brief It loads junctions between segments and perimeters
    /// in Gazebo
    void LoadJunctions();
    /// \brief It loads all waypoints inside the graph structure.
    void LoadWaypoints();
    /// \brief It retrieves the type of waypoint from the vertex
    /// \param[in] vertex It is a vertex reference
    /// \return The waypoint type.
    gazebo::WayPointCreator::Type GetTypeFromVertex(
      std::shared_ptr<ignition::math::Vertex<std::string>> vertex);
    /// \brief It gets the pose of a vertex in the graph.
    /// \param[in] vertex It is a pointer to the vertex to retrieve its pose.
    /// \return The pose of the vertex.
    ignition::math::Pose3d GetPoseFromVertex(
      std::shared_ptr<ignition::math::Vertex<std::string>> vertex);
    /// \brief It gets the following vertex inside a road.
    /// \param[in] vertex It is the pointer to the vertex to get the following
    /// in the lane.
    /// \return A shared_ptr to a vertex inside the graph.
    std::shared_ptr<ignition::math::Vertex<std::string>> GetNextVertex(
      std::shared_ptr<ignition::math::Vertex<std::string>> vertex);
    /// \brief It gets the previous vertex of a road.
    /// \param[in] vertex It is a pointer to a vertex to get its previous
    /// one in the road
    /// \return A shared_ptr to a vertex inside the graph.
    std::shared_ptr<ignition::math::Vertex<std::string>> GetPreviousVertex(
      std::shared_ptr<ignition::math::Vertex<std::string>> vertex);
    /// \brief It gets the pose from two consecutive vertices.
    /// \param[in] vertex A pointer to the first point
    /// \param[in] nextVertex A pointer to the second point
    /// \return The pose of the point that vertex represents heading
    /// to nextVertex
    ignition::math::Pose3d GetPoseFromTwoVertices(
      std::shared_ptr<ignition::math::Vertex<std::string>> vertex,
      std::shared_ptr<ignition::math::Vertex<std::string>> nextVertex);
    /// \brief It gets the pose from two consecutive vertices but heading from
    /// nextVertex to vertex.
    /// \param[in] vertex A pointer to the first point
    /// \param[in] nextVertex A pointer to the second point
    /// \return The pose of the point that vertex represents heading
    /// to nextVertex
    /// \return The pose of the point that vertex represents heading
    /// from vertex to
    /// nextVertex
    ignition::math::Pose3d GetInverseFlowPoseFromTwoVertices(
      std::shared_ptr<ignition::math::Vertex<std::string>> vertex,
      std::shared_ptr<ignition::math::Vertex<std::string>> nextVertex);
    /// \brief It gets the pose with no orientation, just the position.
    /// \param[in] vertex It is the pointer to the vertex to get its pose.
    /// \return A pose with no orientation, just the position.
    ignition::math::Pose3d GetPoseWithoutOrientationFromVertex(
      std::shared_ptr<ignition::math::Vertex<std::string>> vertex);
    /// \brief It finds a waypoint by its unique id
    /// \param[in] waypointId It is an object that represents the id of the
    /// waypoint inside the RNDF file.
    /// \return The pointer to the waypoint if that matches the waypoint id
    manifold::rndf::Waypoint* GetWaypointByUniqueId(
      const manifold::rndf::UniqueId &waypointId);
    /// \brief It gets the lane reference by a valid waypoint into it
    /// \param[in] waypointId It is the reference to the waypoint
    /// \return A reference to the lane if it was found
    /// \throws gazebo::common::Exception if the lane is not found.
    manifold::rndf::Lane* GetLaneByUniqueId(
      const manifold::rndf::UniqueId &waypointId);
    /// \brief It creates a lane name like 'lane_SId_LId'
    /// \param[in] segmentParentId It is the segment parent Id
    /// \param[in] laneId It is the lane Id
    /// \return A std::string in the form 'lane_SId_LId'
    std::string CreateLaneName(const int segmentParentId, const int laneId);
    /// \brief It finds all the lane waypoints, and loads its positions.
    /// \param[in] laneName It is the name of the lane
    /// \param[in] lane A reference to the lane, to get the size
    /// of the waypoints.
    /// \param[out] waypointPositions A vector to fill with the
    /// waypoint positions.
    void FillLaneWaypointPositions(const std::string &laneName,
      const manifold::rndf::Lane &lane,
      std::vector<ignition::math::Vector3d> &waypointPositions);
    /// \brief It fills a vector with all the control points of a road given an
    /// interpolation distance.
    /// \param[in] laneName It is the name of the lane
    /// \param[in] lane A reference to the lane, to get the size of
    /// the waypoints.
    /// \param[out] lanePoints A vector to fill with the interpolated
    /// waypoint positions.
    void FillLanePoints(const std::string &laneName,
      const manifold::rndf::Lane &lane,
      std::vector<ignition::math::Vector3d> &lanePoints);
    /// \brief It fills the a vector with perimeter vertices positions
    /// \param[in] zoneId The id of the zone that contains a perimeter
    /// \param[in] perimeter A reference to the perimeter to get the
    /// number of vertices it has
    /// \param[out] vertices A vector reference to fill with the
    /// vertex positions.
    void FillPerimeterVertices(const int zoneId,
      const manifold::rndf::Perimeter &perimeter,
      std::vector<ignition::math::Vector3d> &vertices);
    /// \brief It parses the SDF file to get the plugin arguments
    /// \param[in] sdfPtr This is the file path of the sdf world
    /// \throws gazebo::Exception in case there is a problem
    /// finding the plugin node.
    void ParseSDF(sdf::ElementPtr sdfPtr);
    /// \brief It loads all the zones attributes.
    /// \param[in] zones A reference to a vector containing the zones
    void LoadZones(std::vector<manifold::rndf::Zone> &zones);
    /// \brief It loads the waypoints, the polygon of the perimeter
    /// and the junctions to it.
    /// \param[in] zoneId It is the id of its paren zone.
    /// \param[in] perimeter A reference to the perimeter.
    void LoadPerimeter(const int zoneId,
      manifold::rndf::Perimeter &perimeter);
    /// \brief It creates the junctions and draws them for the segments
    /// \param[out] junctions A vector, empty or not, with the junctions.
    void GenerateJunctions(
      std::vector<gazebo::Junction> &junctions);
    /// \brief It fills the junction vector or modifies an element of it
    /// given a new vector of exits.
    /// \brief exits A reference to the exits vector
    /// \brief A reference to the junctions vector
    void FillJunctions(
      std::vector<manifold::rndf::Exit> const &exits,
      std::vector<gazebo::Junction> &junctions);
    /// \brief It creates a lane termination object from the unique id
    /// \param[in] id The id of the waypoint to look for.
    /// \return A lane termination object
    gazebo::LaneTermination CreateLaneTermination(
      const manifold::rndf::UniqueId &id);
    /// \brief It gets a vector containing the lat, long and elevation
    /// from the location of a waypoints.
    /// \param[in] wp A waypoint to get its location
    /// \return A vector containing lat, long and elevation coordinates.
    ignition::math::Vector3d GetWaypointSphericalLocation(
      manifold::rndf::Waypoint &wp);
    /// \brief It gets all the spherical locations of all the waypoints
    /// \param[out] A vector to load all the positions.
    void GetAllWaypointLocations(
      std::vector<ignition::math::Vector3d> &positions);

    /// \brief It keeps track of the times the Update function is called
    int count;
    /// \brief It holds the visual message that is created to create waypoints
    msgs::Visual *visualMsg;
    /// \brief It keeps the connections of the handlers like Update function
    std::vector<event::ConnectionPtr> connections;
    /// \brief Utiliy object that creates roads
    gazebo::RoadCreator roadCreator;
    /// \brief Utility object that creates waypoints
    gazebo::WayPointCreator waypointCreator;
    /// \brief Manifold object tha loads into memory the RNDF file
    std::shared_ptr<manifold::rndf::RNDF> rndfInfo;

    std::shared_ptr<manifold::RoadNetwork> roadNetwork;

    /// \brief It is the parsed argument to know if waypoints are required
    bool printWaypoints;
    /// \brief It is the parsed argument to know if waypoints are required
    bool printLanes;
    /// \brief It is the parsed argument to know if perimeters are required
    bool printPerimeter;
    /// \brief It is the parsed argument to know if junctions are required.
    bool printJunctions;
    /// \brief It is the parsed RNDF file path argument
    std::string filePath;
    /// \brief It has the waypoint material string
    std::string waypointMaterial;
    /// \brief It has the lane material string
    std::string laneMaterial;
    /// \brief It has the junction material string
    std::string junctionMaterial;
    /// \brief It has the perimeter material string
    std::string perimeterMaterial;
    /// \brief This is the interpolation distance parameter for the roads.
    double interpolationDistance;
    /// \brief This is the origin to refer the RNDF projection from Global
    /// to Local frames.
    ignition::math::SphericalCoordinates origin;
    /// \brief Used to set the origin in case it appears in the RNDF file.
    bool originIsSet;
    /// \brief Defines labels should be displayed or not
    bool printLabels;
    /// \brief Wrapper to control the camera
    gazebo::CameraController cameraController;
    /// \brief It has the projection methods to convert coordinates
    gazebo::Projection projection;
    // \brief It keeps track of the sdf plugin node
    sdf::ElementPtr sdfPtr;
    gazebo::PerimeterCreator perimeterCreator;
    /// \brief A junction creator to create the junctions
    gazebo::JunctionCreator junctionCreator;
    /// \brief A reference to the parent world pointer.
    gazebo::physics::WorldPtr worldPtr;
    /// \brief Subscriber to get path messages
    transport::SubscriberPtr waypointsRequestsSubscriber;
    /// \brief Transport node used to communicate with the transport system
    transport::NodePtr node;

  protected:
};

}

#endif
