/*
 * Copyright (C) 2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

#ifndef RNDF_GAZEBO_PLUGIN_HH
#define RNDF_GAZEBO_PLUGIN_HH

#include <gazebo/gazebo.hh>
#include <gazebo/util/system.hh>
#include <gazebo/msgs/msgs.hh>
#include <gazebo/rendering/rendering.hh>
#include <gazebo/rendering/RenderTypes.hh>
#include <gazebo/transport/TransportTypes.hh>
#include <gazebo/transport/Publisher.hh>
#include <gazebo/gui/GuiIface.hh>
#include <gazebo/gui/EntityMaker.hh>
#include <gazebo/gui/GuiEvents.hh>
#include <gazebo/math/Vector3.hh>
#include <gazebo/common/common.hh>
#include <gazebo/common/Console.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/rendering/UserCamera.hh>
#include <gazebo/math/Quaternion.hh>
#include <gazebo/common/MouseEvent.hh>
#include <gazebo/common/Exception.hh>
#include <sdf/Element.hh>
#include <ignition/math.hh>
#include <ignition/msgs.hh>
#include <ignition/transport.hh>


#include <manifold/rndf/RNDF.hh>
#include <manifold/rndf/Segment.hh>
#include <manifold/rndf/Lane.hh>
#include <manifold/rndf/Waypoint.hh>
#include <manifold/rndf/Exit.hh>
#include <manifold/rndf/UniqueId.hh>
#include <manifold/rndf/Zone.hh>
#include <manifold/rndf/Perimeter.hh>
#include <manifold/rndf/ParkingSpot.hh>

#include <fstream>
#include <streambuf>
#include <sstream>
#include <chrono>
#include <thread>
#include <string>
#include <vector>
#include <algorithm>

#include "dynamic_render.hh"
#include "rndf_plugin_helpers.hh"
#include "road_creator.hh"
#include "waypoint_creator.hh"
#include "camera_controller.hh"
#include "projection.hh"
#include "perimeter_creator.hh"
#include "junction_creator.hh"

#define DEBUG_MSGS
#ifdef DEBUG_MSGS
  #define DEBUG_MSG(str) do { std::cout << str << std::endl; } while (false)
#else
  #define DEBUG_MSG(str) do { } while (false)
#endif

namespace gazebo {

/// \brief  It is the base class of the plugin which handles the events
/// and methods of the World plugin
class RNDF_PLUGIN_VISIBLE RNDFPlugin : public WorldPlugin {
  public:
    /// \brief Constructor
    RNDFPlugin();
    /// \brief Destructor
    virtual ~RNDFPlugin();
    /// \brief It loads the Update event handler and tries to load the
    ///  RNDF file.
    /// \param[in] _parent It is a reference to the parent world
    /// \param[in] _sdf It is a reference to the plugin node in the world
    /// file definition
    void Load(gazebo::physics::WorldPtr _parent, sdf::ElementPtr _sdf);

  private:

    /// \brief The object that handles all the internal stuff.
    std::shared_ptr<gazebo::DynamicRender> rndfRender;

  protected:
};

  // Register this plugin with the simulator
  GZ_REGISTER_WORLD_PLUGIN(RNDFPlugin)

}

#endif
