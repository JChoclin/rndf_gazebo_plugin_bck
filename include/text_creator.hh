/*
 * Copyright (C) 2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/
#ifndef TEXT_CREATOR_HH
#define TEXT_CREATOR_HH

#include <gazebo/gazebo.hh>
#include <gazebo/util/system.hh>
#include <gazebo/msgs/msgs.hh>
#include <gazebo/rendering/rendering.hh>
#include <gazebo/rendering/RenderTypes.hh>
#include <gazebo/transport/TransportTypes.hh>
#include <gazebo/transport/Publisher.hh>


#include <ignition/math.hh>
#include <ignition/msgs.hh>
#include <ignition/transport.hh>

#include <iostream>
#include <string>
#include <vector>

#include "rndf_plugin_helpers.hh"
#include "creator.hh"

namespace gazebo {

/// \brief This sample class can create text messages with markers
class RNDF_PLUGIN_VISIBLE TextCreator : public Creator {
  public:

    /// \brief Constructor
    TextCreator();
    /// \brief Destructor
    ~TextCreator();
    /// \brief Setter for the name
    void Name(const std::string &_name);
    /// \brief Setter for the material of the text
    void Material(const std::string &_material);
    /// \brief Setter for the text to load into the label
    void Text(const std::string &_text);
    /// \brief Setter for the scale of the label
    void Scale(const ignition::math::Vector3d &_scale);
    /// \brief Setter for the pose of the label
    void Pose(const ignition::math::Pose3d &_pose);
    /// \brief It constructs the label message and then it sends
    /// the message to the markers topic
    bool Create();

  private:

    /// \brief The name of the marker message
    std::string name;
    /// \brief The text to send
    std::string text;
    /// \brief It is the material of the road
    std::string material;
    /// \brief The pose of the text
    ignition::math::Pose3d pose;
    /// \brief The scale of the text
    ignition::math::Vector3d scale;

  protected:
};

}

#endif
