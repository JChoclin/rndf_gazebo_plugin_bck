/*
 * Copyright (C) 2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

#ifndef PROJECTION_HH
#define PROJECTION_HH

#include <gazebo/gazebo.hh>
#include <gazebo/util/system.hh>
#include <gazebo/msgs/msgs.hh>
#include <gazebo/rendering/rendering.hh>
#include <gazebo/rendering/RenderTypes.hh>
#include <gazebo/transport/TransportTypes.hh>
#include <gazebo/transport/Publisher.hh>
#include <gazebo/gui/GuiIface.hh>
#include <gazebo/gui/EntityMaker.hh>
#include <gazebo/gui/GuiEvents.hh>
#include <gazebo/math/Vector3.hh>
#include <gazebo/common/Console.hh>
#include <gazebo/rendering/UserCamera.hh>
#include <gazebo/math/Quaternion.hh>
#include <gazebo/common/MouseEvent.hh>
#include <gazebo/common/Exception.hh>
#include <ignition/math.hh>


#include <manifold/rndf/RNDF.hh>
#include <manifold/rndf/Segment.hh>
#include <manifold/rndf/Lane.hh>
#include <manifold/rndf/Waypoint.hh>

#include <sstream>
#include <chrono>
#include <thread>
#include <string>
#include <vector>

#include "rndf_plugin_helpers.hh"
#include "math_util.hh"

namespace gazebo {

class RNDF_PLUGIN_VISIBLE Projection {
  public:

    /// \brief Constructor
    Projection();
    /// \brief Destructor
    ~Projection();
    /// \brief It converts a coordinate from Spherical to ECEF. It
    /// uses the center as the reference.
    /// \param[in] sc It is the spherical coordinate. x = latitude,
    /// y = longitude and z = elevation.
    /// \return A ignition::mat::Vector3d with ECEF coordinates.
    ignition::math::Vector3d SphericalToECEF(
      ignition::math::Vector3d &sc);
    /// \brief It converts the waypoint spherical coordinates to
    /// Gazebo position coordinates in GLOBAL frame reference
    /// \param[in] waypoint It is the waypoint to compute its position
    /// \return A lat, long, elev and headerin object with the
    /// representation of the plane
    ignition::math::SphericalCoordinates GetCenterOfThePlane();
    /// \brief It converts a spherical coordinate to
    /// Gazebo position coordinates in GLOBAL frame reference.
    /// \param[in] sc It is the spherical coordinate to transform.
    /// \return ignition::math::Vector3d with the position in
    /// GLOBAL (north, east, up) coordinates.
    ignition::math::Vector3d SphericalToGlobal(
      ignition::math::Vector3d &sc);
    /// \brief It goes throw all the waypoints and gets the extents
    /// of the city
    /// \details The steps are:
    /// 1. Get the minimum and maximum lat, long, elev coordiante
    /// 2. Generate the center in SPHERICAL coordiantes (lat, long, elev)
    /// 3. Convert the coordinates in SPHERICAL into GLOBAL taken the previous
    /// center as reference.
    /// 4. Get the maximum and minimum in X, Y, and Z in GLOBAL frame refernece.
    /// \param[in] locations It a vector containing all the locations to get the
    /// center and the extents.
    void GetRNDFSpaceLimits(
      std::vector<ignition::math::Vector3d> &locations);
    /// \brief It goes throw all the waypoints and gets the extents
    /// of the city
    /// \details The steps are:
    /// 1. Get the minimum and maximum lat, long, elev coordiante
    /// 2. Generate the center in SPHERICAL coordiantes (lat, long, elev)
    /// 3. Convert the coordinates in SPHERICAL into GLOBAL taken the previous
    /// center as reference.
    /// 4. Get the maximum and minimum in X, Y, and Z in GLOBAL frame refernece.
    /// \param[in] locations It a vector containing all the locations to get the
    /// center and the extents.
    /// \param[in] The new origin to assign.
    void GetRNDFSpaceLimits(
      std::vector<ignition::math::Vector3d> &locations,
      ignition::math::SphericalCoordinates &newOrigin);
    /// \brief Getter of x_max
    /// \return x_max
    double GetXMax();
    /// \brief Getter of x_min
    /// \return x_min
    double GetXMin();
    /// \brief Getter of y_max
    /// \return y_max
    double GetYMax();
    /// \brief Getter of y_min
    /// \return y_min
    double GetYMin();
    /// \brief Getter of z_max
    /// \return z_max
    double GetZMax();
    /// \brief Getter of z_min
    /// \return z_min
    double GetZMin();
    /// \brief Getter of vector of minimum point
    /// \return Returns a vector composed of x_min, y_min, z_min
    ignition::math::Vector3d GetMin();
    /// \brief Getter of vector of maximum point
    /// \return Returns a vector composed of x_max, y_max, z_max
    ignition::math::Vector3d GetMax();
    /// \brief Accessor to the center variable.
    ignition::math::SphericalCoordinates& Center();
    /// \brief Accessor to the origin variable.
    ignition::math::SphericalCoordinates& Origin();

  private:
    /// \brief It creates the center as a SphicalCoordiante object
    void GenerateCenterOfThePlane();
    /// \brief This method checks if the new position is is inside the
    /// the current limits of the city and updtes the extents if not
    /// \param[in] position The new position vector to check
    void CheckCityLimitsLatLong(const ignition::math::Vector3d &position);
    /// \brief This method checks if the new position is is inside the
    /// the current limits of the city and updtes the extents if not
    /// \param[in] position The new position vector to check
    void CheckCityLimits(const ignition::math::Vector3d &position);

    /// \brief It keeps track of the minimum projection of the waypoints
    /// in x direction
    double x_min;
    /// \brief It keeps track of the maximum projection of the waypoints
    /// in x direction
    double x_max;
    /// \brief It keeps track of the minimum projection of the waypoints
    /// in y direction
    double y_min;
    /// \brief It keeps track of the maximum projection of the waypoints
    /// in y direction
    double y_max;
    /// \brief It keeps track of the minimum projection of the waypoints
    /// in z direction
    double z_min;
    /// \brief It keeps track of the maximum projection of the waypoints
    /// in z direction
    double z_max;
    /// \brief It keeps track of the minimum latitude
    double lat_min;
    /// \brief It keeps track of the maximum latitude
    double lat_max;
    /// \brief It keeps track of the minimum longitude
    double long_min;
    /// \brief It keeps track of the maximum longitude
    double long_max;
    /// \brief It keeps track of the maximum elevation
    double elev_max;
    /// \brief It keeps track of the minimum elevation
    double elev_min;
    /// \brief It keeps track of the center latitude
    double lat_center;
    /// \brief It keeps track of the center longitude
    double long_center;
    /// \brief It keeps track of the center elevation
    double elev_center;
    /// \brief It keeps track of the center of the map in
    /// spherical coordinates.
    ignition::math::SphericalCoordinates center;
    /// \brief It keeps the origin of projection of
    /// spherical coordinates
    ignition::math::SphericalCoordinates origin;
};

}

#endif
