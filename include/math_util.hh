/*
 * Copyright (C) 2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/
#ifndef MATH_UTIL_HH
#define MATH_UTIL_HH

#include <gazebo/gazebo.hh>
#include <ignition/math.hh>

#include <vector>
#include <algorithm>
#include <cmath>

#include "rndf_plugin_helpers.hh"

namespace gazebo {

/// \brief It is used to sort by angle the points to the center
/// of them.
struct RNDF_PLUGIN_VISIBLE PolarSort{
  /// \brief Constructor
  /// \param[in] A reference to the the center of the junction
  explicit PolarSort(ignition::math::Vector3d &c) : center(c) {}
  /// \brief The center of the points
  ignition::math::Vector3d center;
  /// \brief Function operator to be called when comparing
  /// the angles with respect to the center.
  /// \param[in] a A reference to a position.
  /// \param[in] b A reference to other position.
  /// \return true if the angle of A is smaller than the
  /// angle of B
  bool operator()(const ignition::math::Vector3d &a,
    const ignition::math::Vector3d &b) {
    double angleA = std::atan2(a.Y() - center.Y(),
      a.X() - center.X());
    double angleB = std::atan2(b.Y() - center.Y(),
      b.X() - center.X());
    return angleA < angleB;
  }
};

ignition::math::Vector3d RNDF_PLUGIN_VISIBLE GetCenter(
  std::vector<ignition::math::Vector3d> &points);

void RNDF_PLUGIN_VISIBLE OrderPolar(
  std::vector<ignition::math::Vector3d> &points);

void RNDF_PLUGIN_VISIBLE ComputeExtents(
  const ignition::math::Vector3d &position,
  const double angle,
  const double distance,
  ignition::math::Vector3d &a,
  ignition::math::Vector3d &b);

ignition::math::SphericalCoordinates RNDF_PLUGIN_VISIBLE
  GetSphericalCoordinates(const double latitude,
    const double longitude, const double elev);

double RNDF_PLUGIN_VISIBLE RadianToDegree(
  const double angle);

double RNDF_PLUGIN_VISIBLE DegreeToRadian(
  const double angle);

}

#endif
