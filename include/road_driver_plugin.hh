/*
 * Copyright (C) 2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/
#ifndef RNDF_DRIVER_HH
#define RNDF_DRIVER_HH

#include <boost/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/bind.hpp>

#include <ctime>
#include <mutex>

#include <string>
#include <vector>


#include <sdf/Param.hh>
#include <gazebo/transport/TransportTypes.hh>
#include <gazebo/msgs/MessageTypes.hh>
#include <gazebo/common/Time.hh>
#include <gazebo/common/Plugin.hh>
#include <gazebo/common/Events.hh>

#include <gazebo/physics/physics.hh>
#include <gazebo/transport/transport.hh>
#include <gazebo/common/common.hh>
#include <gazebo/gazebo.hh>

#include "./waypoints_request.pb.h"
#include "./waypoints_response.pb.h"

namespace gazebo
{

  static double const defaultP = 5.0;
  static double const defaultI = 0.0;
  static double const defaultD = 0.0;

  class RoadDriver : public ModelPlugin
  {
    typedef const boost::shared_ptr<
      const rndf_gazebo_plugin_msgs::msgs::WaypointsResponse>
      WaypointsResponsePtr;

    /// \brief Pointer to the model world
    private: physics::WorldPtr world;

    /// \brief connection pointer for world update function
    private: event::ConnectionPtr updateConnection_;

    /// \brief topic name
    private: std::string rndf_topic;

    /// \brief name of the model
    private: std::string model_name;

    /// \brief parent name
    private: std::string link_name;

    /// \brief Constructor
    public: RoadDriver();

    /// \brief Destructor
    public: ~RoadDriver();

    /// \brief Plugin Load function
    /// \param[in] _parent Model pointer to the model defining this plugin
    /// \param[in] _sdf pointer to the SDF of the model
    public: void Load(physics::ModelPtr _parent, sdf::ElementPtr _sdf);

    /// \brief World update function
    public: void OnUpdate(const common::UpdateInfo&);

    /// \brief update function
    public: void Reset();

    /// \brief Perform movement of the model
    /// \param postitionTarget pose that describes the position of the next
    //  desired position and the  desired heading
    private: void Move(math::Pose positionTarget);

    /// \brief Define next entry
    private: void DefineNextEntry();

    /// \brief Select next lanePoint
    private: math::Pose DefineNextLanePointPosition();

    /// \brief Send request of lanePoints to server
    /// \param ID from the next entry of the next lane
    private: void RequestWaypoints(math::Vector3 lanePointID);

    /// \brief Callback to run when recieve a path message.
    /// \param[in] _msg path message received to animate.
    public: void OnWaipointsResponseMsg(WaypointsResponsePtr &_msg);

    /// \brief Pointer to the model that defines this plugin
    private: physics::ModelPtr model;

    /// \brief pointer to the move animation to perform
    private: gazebo::common::PoseAnimationPtr anim;

    /// \brief Transport node used to communicate with the transport system
    private: transport::NodePtr node;

    /// \brief Subscriber to get path messages
    private: transport::SubscriberPtr pathSubscriber;

    /// \ Waipoint request publisher
    private: gazebo::transport::PublisherPtr waypointsRequestPub;

    /// \brief Starting point of the path to follow
    private: math::Vector3 startPosition;

    /// \brief Minimal distance to consider to arrive to a LanePoint
    private: double minimalDistance;

    /// \brief Update period for the model velocity
    private: double updatePeriod;

    /// \brief Max. linear velocity to be used
    private: double linearVelocity;

    /// \brief Max. linear velocity to be used
    private: double angularVelocity;

    /// \brief last time OnUpdate function took action
    private: common::Time lastTime;

    /// \brief time to start updates, waiting for gazebo to be up
    private: common::Time startTime;

    /// \brief Variable used to know if there is position jump to
    /// the first lanePoint
    private: bool firstLanePoint;

    /// \brief wait for response
    private: bool waitResponse;

    /// \brief initial lanePoint specified in the SDF
    private: math::Vector3 initialLanePointID;

    /// \brief initial time to wait until gazebo is completely brought up
    private: double initialWaitTime;

    /// \brief last entry used that describes the beginning of the current lane
    private: math::Vector3 lastEntry;

    /// \brief Next entry to be used
    private: math::Vector3 currentEntry;

    /// \brief Ids of the next possible entries
    private: std::vector<math::Vector3> entriesIds;

    /// \brief Positions of the nest posible entries
    private: std::vector<math::Vector3> entriesPositions;

    /// \brief Ids of the next possible exits
    private: std::vector<math::Vector3> exitIds;

    /// \brief Path to follow
    private: std::vector<math::Vector3> pathLanePoints;

    /// \brief Positions of the waypoints contained in the current lane
    private: std::vector<math::Vector3> waypointsPositions;

    /// \brief Ids of the waypoints contained in the current lane
    private: std::vector<math::Vector3> waypointsIds;

    /// \brief PID for X axis
    private: common::PID axisXPID;

    /// \brief PID for Y axis
    private: common::PID axisYPID;

    /// \brief PID for Z axis
    private: common::PID axisZPID;

    /// \brief PID for roll angle
    private: common::PID axisRollPID;

    /// \brief PID for pitch angle
    private: common::PID axisPitchPID;

    /// \brief PID for yaw angle
    private: common::PID axisYawZPID;

    private: std::mutex mutex;
  };
}
#endif
