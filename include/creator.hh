/*
 * Copyright (C) 2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

#ifndef CREATOR_HH
#define CREATOR_HH

#include <gazebo/gazebo.hh>

#include <ignition/msgs.hh>
#include <ignition/transport.hh>

#include <string>

#include "rndf_plugin_helpers.hh"

namespace gazebo {

/// \brief This class contains common variables for all
/// the creators and will be a base class for other creators.
class RNDF_PLUGIN_VISIBLE Creator {
  public:
    /// \brief It should be used to elevate all the
    /// visuals from the floor
    static const double DEFAULT_HEIGHT;
    /// \brief It should be used to elevate all the
    /// junctions from the floor
    static const double DEFAULT_JUNCTIONS_HEIGHT;
    /// \brief It is the height of the waypoints in z direction
    static const double DEFAULT_WAYPOINT_HEIGHT;
    /// \brief It is the default value of radius for waypoints.
    static const double DEFAULT_WAYPOINT_RADIUS;
    /// \brief It is used when lane width is 0.0
    static const double DEFAULT_LANE_WIDTH;

    static const double DEFAULT_INTERPOLATION_DISTANCE;
    /// \brief The name of the topic to publish the marker messages.
    static const std::string MARKER_TOPIC;
    /// \brief The default material for waypoints.
    static const std::string WAYPOINT_DEFAULT_MATERIAL;
    /// \brief The material used for the texts
    static const std::string TEXT_MATERIAL;
    /// \brief The default offset for the z coordinate used for the texts.
    static const double LANE_TEXT_Z_OFFSET;
    /// \brief The default offset for the z coordinate used for the texts.
    static const double WAYPOINT_TEXT_Z_OFFSET;
    /// \brief The number of segments of road to have in between each lane
    /// name text.
    static const uint NAME_INTERPOLATION_STEPS;
    /// \brief The scale factor for the waypoint name.
    static const double WAYPOINT_NAME_SCALE;
    /// \brief The scale factor for the lane name.
    static const double LANE_NAME_SCALE;
    /// \brief The default material for the lanes.
    static const std::string LANE_DEFAULT_MATERIAL;
    /// \brief The default material for the junctions.
    static const std::string JUNCTION_DEFAULT_MATERIAL;
    /// \brief The default material for the perimeter
    static const std::string PERIMETER_DEFAULT_MATERIAL;

    /// \brief It gets a marker id always incremental
    /// \return A unique incremental id.
    static uint64_t GetMarkerId();


    /// \brief It is a functor for the callback of each waypoint creation
    /// message.
    struct RNDF_PLUGIN_VISIBLE Handler {
      /// \brief Constructor
      explicit Handler() {}
      /// \brief The handler itself, just notifies the result in
      /// case of error.
      /// \param[in] response The response message
      /// \param[in] result The result of the message sent.
      void operator()(const ignition::msgs::StringMsg &response,
        const bool result) {
        if (!result) {
          gzerr << "HandlerMessage: " << response.data() <<
            " | Result: " << result <<
            " | Info: " << info <<
            std::endl;
        }
      }
      void Info(const std::string &_info) {
        this->info = _info;
      }
      std::string info;
    };
    /// \brief Constructor
    Creator();
    /// \brief Destructor
    ~Creator();
    /// \brief Interface for creating a marker message and sending it
    virtual bool Create() = 0;

  private:
    /// \brief It is an incremental counter for the markers values.
    static uint64_t markerId;

  protected:
    /// \brief It creates a std::function as a handler of the
    /// Request API.
    /// \param[in] info It is a string to add information.
    /// \return A std::function to send to the
    /// ignition::transport::Node::Request method.
    std::function<void(const ignition::msgs::StringMsg &,
    const bool)> CreateHandlerFunctor(const std::string info = "");
    /// \brief Ignition transport node used for sending messages to markers
    std::shared_ptr<ignition::transport::Node> nodePtr;
};

}

#endif
