/*
 * Copyright (C) 2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

#ifndef RNDF_PLUGIN_HELPERS_HH_
#define RNDF_PLUGIN_HELPERS_HH_

/// \def RNDF_PLUGIN_VISIBLE
/// Use to represent "symbol visible" if supported

/// \def RNDF_PLUGIN_HIDDEN
/// Use to represent "symbol hidden" if supported

#if defined BUILDING_STATIC_LIBS
  #define RNDF_PLUGIN_VISIBLE
  #define RNDF_PLUGIN_HIDDEN
#else
  #if defined __WIN32 || defined __CYGWIN__
    #ifdef BUILDING_DLL
      #ifdef __GNUC__
        #define RNDF_PLUGIN_VISIBLE __attribute__ ((dllexport))
      #else
        #define RNDF_PLUGIN_VISIBLE __declspec(dllexport)
      #endif
    #else
      #ifdef __GNUC__
        #define RNDF_PLUGIN_VISIBLE __attribute__ ((dllimport))
      #else
        #define RNDF_PLUGIN_VISIBLE __declspec(dllimport)
      #endif
    #endif
    #define RNDF_PLUGIN_HIDDEN
  #else
    #if __GNUC__ >= 4
      #define RNDF_PLUGIN_VISIBLE __attribute__ ((visibility ("default")))
      #define RNDF_PLUGIN_HIDDEN  __attribute__ ((visibility ("hidden")))
    #else
      #define RNDF_PLUGIN_VISIBLE
      #define RNDF_PLUGIN_HIDDEN
    #endif
  #endif
// BUILDING_STATIC_LIBS
#endif

#endif
