/*
 * Copyright (C) 2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

#ifndef CAMERA_CONTROLLER_HH
#define CAMERA_CONTROLLER_HH

#include <gazebo/gui/GuiIface.hh>
#include <gazebo/gazebo.hh>
#include <gazebo/util/system.hh>
#include <gazebo/rendering/UserCamera.hh>
#include <gazebo/rendering/rendering.hh>
#include <gazebo/rendering/RenderTypes.hh>
#include <gazebo/transport/TransportTypes.hh>
#include <gazebo/transport/Publisher.hh>
#include <gazebo/math/Pose.hh>
#include <ignition/math.hh>

#include <iostream>
#include <string>
#include <vector>
#include <cmath>

#include "rndf_plugin_helpers.hh"

namespace gazebo {

class RNDF_PLUGIN_VISIBLE CameraController {
  public:

    CameraController();
    ~CameraController();
    void SetExtents(ignition::math::Vector3d const &min,
      ignition::math::Vector3d const &max);
    void AdjustCamera();
    void AdjustCamera(const ignition::math::Vector3d &axis);
    void SetToDefaultPose();
    ignition::math::Pose3d GetCurrentPose();

  private:

    void UpdateCameraPtr();

    double x_max;
    double y_max;
    double x_min;
    double y_min;
    double z_min;
    double z_max;
    rendering::UserCameraPtr camera;
};

}
#endif
