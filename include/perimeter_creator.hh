/*
 * Copyright (C) 2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

#ifndef PERIMETER_CREATOR_HH
#define PERIMETER_CREATOR_HH

#include <gazebo/gazebo.hh>
#include <gazebo/rendering/rendering.hh>
#include <ignition/math.hh>
#include <ignition/msgs.hh>
#include <ignition/transport.hh>

#include <iostream>
#include <string>
#include <vector>

#include "rndf_plugin_helpers.hh"
#include "creator.hh"

namespace gazebo {

/// \brief It handles the creation of a perimeter polygon. It
/// uses markers triangles to accomplish it.
class RNDF_PLUGIN_VISIBLE PerimeterCreator : public Creator {
  public:
    /// \brief Constructor
    PerimeterCreator();
    /// \brief Destructor
    ~PerimeterCreator();
    /// \brief Setter for the material of the perimeter
    /// \param[in] material A string representation of the material
    void Material(const std::string &material);
    /// \brief Setter for the verteces of the perimeter
    /// \param[in] verteces The positions of all the verteces.
    void Verteces(std::vector<ignition::math::Vector3d> &verteces);
    /// \brief It creates a convex polygon given the a set of
    /// verteces. It works with concave polygons too, but the
    /// baricenter of the polygon must be inside the perimeter.
    /// \return true if the message is sent correctly, false if not
    bool Create();

  private:
    /// \brief It is the material of the perimeter
    std::string material;
    /// \brief These are the verteces of the perimeter
    std::vector<ignition::math::Vector3d> verteces;
};

}
#endif
