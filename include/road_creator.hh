/*
 * Copyright (C) 2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/
#ifndef ROAD_CREATOR_HH
#define ROAD_CREATOR_HH

#include <gazebo/gazebo.hh>
#include <gazebo/util/system.hh>
#include <gazebo/msgs/msgs.hh>
#include <gazebo/rendering/rendering.hh>
#include <gazebo/rendering/RenderTypes.hh>
#include <gazebo/transport/TransportTypes.hh>
#include <gazebo/transport/Publisher.hh>


#include <ignition/math.hh>
#include <ignition/msgs.hh>
#include <ignition/transport.hh>

#include <iostream>
#include <string>
#include <vector>

#include "rndf_plugin_helpers.hh"
#include "text_creator.hh"
#include "creator.hh"
#include "math_util.hh"

namespace gazebo {

/// \brief  Utility class to make road visuals on Gazebo
class RNDF_PLUGIN_VISIBLE RoadCreator : public Creator {
  public:
    /// \brief Default constructor.
    RoadCreator();
    /// \brief Destructor.
    ~RoadCreator();
    /// \brief It creates an interpolated road with splines.
    /// It is recursive to match the minimum distance.
    /// \param[in] points the control points of the splines
    /// \param[in] distanceThreshold it is the maximum distance between
    /// two points of the road
    /// \return A vector of points that is created with splines and matches
    /// the maximum distance between all.
    std::vector<ignition::math::Vector3d> InterpolateRoad(
      const std::vector<ignition::math::Vector3d> &_points,
      const double distanceThreshold);
    /// \brief Setter for the width
    /// \param[in] _width the road width
    void Width(const double _width);
    /// \brief Setter for the name
    /// \param[in] _name the road visual name
    void Name(const std::string &_name);
    /// \brief Setter for the points
    /// \param[in] _points vector of points that make the path of the road
    void Points(std::vector<ignition::math::Vector3d> &_points);
    /// \brief Setter for the material
    /// \param[in] _material The material to add to the road.
    void Material(const std::string &_material);
    /// \brief Sets if labels should be displayed or not.
    void DisplayNames(const bool _printLabels);
    /// \brief It creates a road message
    /// \return True if it can succesfuly send the message.
    bool Create();
    /// \brief Given a vector of points that matches the roads, it
    /// gets a vector of poses of each.
    /// \param[in] _points It is a vector of 3D points that corresponds to
    /// a road.
    /// \param[out] poses It should be an empty vector in which this function
    /// includes the pose of the points. The direction is acquired by doing
    /// a difference vector between two consecutive points. The last point gets
    /// the same direction as the last but one.
    void FillPoses(
      const std::vector<ignition::math::Vector3d> &_points,
      std::vector<ignition::math::Pose3d> &poses);
    /// \brief Given a poses and the width of the lane, it computes
    /// the points and orders them in such a way that the can be rendererd
    /// as a triangle list.
    /// \param[in] laneWidth The width of the lane
    /// \param[in] poses The vector of poses
    /// \param[out] trianglePoints A vector in which to include the calculated
    /// points
    void FillTriangleStripPoints(const double laneWidth,
      const std::vector<ignition::math::Pose3d> &poses,
      std::vector<ignition::math::Vector3d> &trianglePoints);

  private:
    /// \brief It calls CreateName every Creator::NAME_INTERPOLATION_STEPS
    /// and in case the number of control points from the last one is
    /// at least half Creator::NAME_INTERPOLATION_STEPS.
    /// \param[in] poses This are the values of the poses of each control point
    void CreateNames(const std::vector<ignition::math::Pose3d> &poses);
    /// \brief It creates a label for the lane over it.
    /// \brief pose It is the pose of the control point of the road.
    void CreateName(const ignition::math::Pose3d &pose);

    /// \brief It is the width of the road
    double width;
    /// \brief These are the points of the road
    std::vector<ignition::math::Vector3d> points;
    /// \brief It is the name of the lane
    std::string name;
    /// \brief It is the material of the road
    std::string material;
    /// \brief Text creator object for waypoint labels
    gazebo::TextCreator textCreator;
    /// \brief Defines labels should be displayed or not
    bool printLabels;

  protected:
};

}
#endif
