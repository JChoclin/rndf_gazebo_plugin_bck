/*
 * Copyright (C) 2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/
#include "projection.hh"

namespace gazebo {

Projection::Projection() {
  center = GetSphericalCoordinates(0.0, 0.0, 0.0);
  origin = GetSphericalCoordinates(0.0, 0.0, 0.0);
}

Projection::~Projection() {
}

ignition::math::SphericalCoordinates Projection::GetCenterOfThePlane() {
  return center;
}

ignition::math::Vector3d Projection::SphericalToECEF(
  ignition::math::Vector3d &sc) {
  // This should be origin
  return origin.PositionTransform(sc,
    ignition::math::SphericalCoordinates::SPHERICAL,
    ignition::math::SphericalCoordinates::ECEF);
}

ignition::math::Vector3d Projection::SphericalToGlobal(
  ignition::math::Vector3d &sc) {
  // WARNING: Rough approximation. We should use the code in
  // the following comment block instead of this implementation.
  // We will keep this implementation until
  // https://github.com/ekumenlabs/terminus/issues/171 is fixed.
  /*
  return origin.PositionTransform(sc,
    ignition::math::SphericalCoordinates::SPHERICAL,
    ignition::math::SphericalCoordinates::GLOBAL);
  */
  // Transform the origin lat and long to degrees and the incomming
  // point
  ignition::math::Vector3d originInDegrees(
    origin.LatitudeReference().Degree(),
    origin.LongitudeReference().Degree(),
    0.0);
  ignition::math::Vector3d scInDegrees;
  scInDegrees.X() = gazebo::RadianToDegree(sc.X());
  scInDegrees.Y() = gazebo::RadianToDegree(sc.Y());

  const double metersPerDegreeLatitude = 111319.9;
  const double metersPerDegreeLongitude =
    metersPerDegreeLatitude * std::cos(sc.X());

  double deltaLatitudeInDegrees =
    scInDegrees.X() - originInDegrees.X();
  double deltaLongitudeInDegrees =
    scInDegrees.Y() - originInDegrees.Y();

  ignition::math::Vector3d result(0.0, 0.0, 0.0);
  // Get the latitude conversion
  result.Y() = metersPerDegreeLatitude * deltaLatitudeInDegrees;
  // Get the longitude conversion
  if (deltaLongitudeInDegrees >= 180.0) {
    deltaLongitudeInDegrees = deltaLongitudeInDegrees - 360.0;
  }
  if (deltaLongitudeInDegrees < -180.0) {
    deltaLongitudeInDegrees = deltaLongitudeInDegrees + 360.0;
  }
  result.X() =
    deltaLongitudeInDegrees * metersPerDegreeLongitude;
  // Corner value detection
  if (scInDegrees.Y() == 90.0 && scInDegrees.X() == 0.0) {
    result.X() = 0.0;
  }
  return result;
}


void Projection::GetRNDFSpaceLimits(
  std::vector<ignition::math::Vector3d> &locations) {
  // Preload values before finding the maximun and minimum
  {
    lat_min = lat_max = locations[0].X();
    long_min = long_max = locations[0].Y();
    elev_min = elev_max = locations[0].Z();
  }
  for (ignition::math::Vector3d &location : locations) {
    CheckCityLimitsLatLong(location);
  }
  // Get the coordinates that matches center of the extents
  lat_center = (lat_max + lat_min) / 2.0;
  long_center = (long_max + long_min) / 2.0;
  elev_center = (elev_max + elev_min) / 2.0;
  GenerateCenterOfThePlane();
  // Set the new origin
  origin = center;
  // Preload values before finding the maximun and minimum
  ignition::math::Vector3d min = origin.PositionTransform(
    ignition::math::Vector3d(lat_min, long_min, elev_min),
    ignition::math::SphericalCoordinates::SPHERICAL,
    ignition::math::SphericalCoordinates::GLOBAL);
  ignition::math::Vector3d max = origin.PositionTransform(
    ignition::math::Vector3d(lat_max, long_max, elev_max),
    ignition::math::SphericalCoordinates::SPHERICAL,
    ignition::math::SphericalCoordinates::GLOBAL);
  x_min = min.X();
  y_min = min.Y();
  z_min = min.Z();
  x_max = max.X();
  y_max = max.Y();
  z_max = max.Z();
}

void Projection::GetRNDFSpaceLimits(
  std::vector<ignition::math::Vector3d> &locations,
  ignition::math::SphericalCoordinates &newOrigin) {
  // Preload values before finding the maximun and minimum
  {
    lat_min = lat_max = locations[0].X();
    long_min = long_max = locations[0].Y();
    elev_min = elev_max = locations[0].Z();
  }
  for (ignition::math::Vector3d &location : locations) {
    CheckCityLimitsLatLong(location);
  }
  // Get the coordinates that matches center of the extents
  lat_center = (lat_max + lat_min) / 2.0;
  long_center = (long_max + long_min) / 2.0;
  elev_center = (elev_max + elev_min) / 2.0;
  GenerateCenterOfThePlane();
  // Set the new origin
  origin = newOrigin;
  // Preload values before finding the maximun and minimum
  ignition::math::Vector3d min = origin.PositionTransform(
    ignition::math::Vector3d(lat_min, long_min, elev_min),
    ignition::math::SphericalCoordinates::SPHERICAL,
    ignition::math::SphericalCoordinates::GLOBAL);
  ignition::math::Vector3d max = origin.PositionTransform(
    ignition::math::Vector3d(lat_max, long_max, elev_max),
    ignition::math::SphericalCoordinates::SPHERICAL,
    ignition::math::SphericalCoordinates::GLOBAL);
  x_min = min.X();
  y_min = min.Y();
  z_min = min.Z();
  x_max = max.X();
  y_max = max.Y();
  z_max = max.Z();
}

double Projection::GetXMax() {
  return x_max;
}

double Projection::GetXMin() {
  return x_min;
}

double Projection::GetYMax() {
  return y_max;
}

double Projection::GetYMin() {
  return y_min;
}

double Projection::GetZMax() {
  return z_max;
}

double Projection::GetZMin() {
  return z_min;
}

ignition::math::Vector3d Projection::GetMin() {
  return ignition::math::Vector3d(x_min, y_min, z_min);
}

ignition::math::Vector3d Projection::GetMax() {
  return ignition::math::Vector3d(x_max, y_max, z_max);
}
void Projection::CheckCityLimitsLatLong(
  const ignition::math::Vector3d &position) {
    lat_min = std::min(lat_min, position.X());
    lat_max = std::max(lat_max, position.X());
    long_min = std::min(long_min, position.Y());
    long_max = std::max(long_max, position.Y());
    elev_min = std::min(elev_min, position.Z());
    elev_max = std::max(elev_max, position.Z());
}

void Projection::CheckCityLimits(const ignition::math::Vector3d &position) {
    x_min = std::min(x_min, position.X());
    x_max = std::max(x_max, position.X());
    y_min = std::min(y_min, position.Y());
    y_max = std::max(y_max, position.Y());
    z_min = std::min(z_min, position.Z());
    z_max = std::max(z_max, position.Z());
}

void Projection::GenerateCenterOfThePlane() {
  center = GetSphericalCoordinates(
    RadianToDegree(lat_center),
    RadianToDegree(long_center),
    elev_center);
}

ignition::math::SphericalCoordinates& Projection::Center() {
  return center;
}

ignition::math::SphericalCoordinates& Projection::Origin() {
  return origin;
}

}
