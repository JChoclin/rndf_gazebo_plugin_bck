/*
 * Copyright (C) 2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

#include <gazebo/gazebo.hh>
#include <ignition/math.hh>
#include "math_util.hh"

#include "gtest/gtest.h"

#define DOUBLE_TOLERANCE  0.00001

TEST(MATH_UTIL_Test, ComputeExtents)
{
  ignition::math::Vector3d position(0.0, 0.0, 0.0);
  double angle, distance;

  {
    position = ignition::math::Vector3d(0.0, 0.0, 0.0);
    angle = 0.0;
    distance = 1.0;
    ignition::math::Vector3d a, b;
    gazebo::ComputeExtents(position,
      angle,
      distance,
      a,
      b);
    EXPECT_EQ(a, ignition::math::Vector3d(0.5, 0.0, 0.0));
    EXPECT_EQ(b, ignition::math::Vector3d(-0.5, 0.0, 0.0));
  }


  {
    position = ignition::math::Vector3d(0.0, 0.0, 0.0);
    angle = M_PI/2.0;
    distance = 1.0;
    ignition::math::Vector3d a, b;
    gazebo::ComputeExtents(position,
      angle,
      distance,
      a,
      b);
    EXPECT_EQ(a, ignition::math::Vector3d(0.0, 0.5, 0.0));
    EXPECT_EQ(b, ignition::math::Vector3d(0.0, -0.5, 0.0));
  }
}

TEST(MATH_UTIL_Test, RadianToDegree)
{
  double angle;

  angle = M_PI / 4.0;
  EXPECT_NEAR(gazebo::RadianToDegree(angle),
    45.0,
    DOUBLE_TOLERANCE);

  angle = M_PI / 6.0;
  EXPECT_NEAR(gazebo::RadianToDegree(angle),
    30.0,
    DOUBLE_TOLERANCE);

  angle = M_PI / 2.0;
  EXPECT_NEAR(gazebo::RadianToDegree(angle),
    90.0,
    DOUBLE_TOLERANCE);

  angle = M_PI / 3.0;
  EXPECT_NEAR(gazebo::RadianToDegree(angle),
    60.0,
    DOUBLE_TOLERANCE);

  angle = M_PI / 3.0 * 2.0;
  EXPECT_NEAR(gazebo::RadianToDegree(angle),
    120.0,
    DOUBLE_TOLERANCE);

  angle = M_PI / 4.0 * 3.0;
  EXPECT_NEAR(gazebo::RadianToDegree(angle),
    135.0,
    DOUBLE_TOLERANCE);

  angle = M_PI;
  EXPECT_NEAR(gazebo::RadianToDegree(angle),
    180.0,
    DOUBLE_TOLERANCE);

  angle = -M_PI / 4.0;
  EXPECT_NEAR(gazebo::RadianToDegree(angle),
    -45.0,
    DOUBLE_TOLERANCE);
}

TEST(MATH_UTIL_Test, DegreeToRadian)
{
  double angle;

  angle = 45.0;
  EXPECT_NEAR(gazebo::DegreeToRadian(angle),
     M_PI / 4.0,
    DOUBLE_TOLERANCE);

  angle = 30.0;
  EXPECT_NEAR(gazebo::DegreeToRadian(angle),
    M_PI / 6.0,
    DOUBLE_TOLERANCE);

  angle = 90.0;
  EXPECT_NEAR(gazebo::DegreeToRadian(angle),
    M_PI / 2.0,
    DOUBLE_TOLERANCE);

  angle = 60.0;
  EXPECT_NEAR(gazebo::DegreeToRadian(angle),
    M_PI / 3.0,
    DOUBLE_TOLERANCE);

  angle = 120.0;
  EXPECT_NEAR(gazebo::DegreeToRadian(angle),
    M_PI / 3.0 * 2.0,
    DOUBLE_TOLERANCE);

  angle = 135.0;
  EXPECT_NEAR(gazebo::DegreeToRadian(angle),
    M_PI / 4.0 * 3.0,
    DOUBLE_TOLERANCE);

  angle = 180.0;
  EXPECT_NEAR(gazebo::DegreeToRadian(angle),
    M_PI,
    DOUBLE_TOLERANCE);

  angle = -45.0;
  EXPECT_NEAR(gazebo::DegreeToRadian(angle),
    -M_PI / 4.0,
    DOUBLE_TOLERANCE);
}

TEST(MATH_UTIL_Test, GetSphericalCoordinates)
{
  double latitude = 45.0;
  double longitude = -60.0;
  double elevation = 12.3;
  ignition::math::SphericalCoordinates sc =
    gazebo::GetSphericalCoordinates(latitude,
      longitude,
      elevation);
  EXPECT_EQ(sc.HeadingOffset(), ignition::math::Angle::Zero);
  EXPECT_NEAR(sc.ElevationReference(),
    elevation,
    DOUBLE_TOLERANCE);
  EXPECT_NEAR(sc.LatitudeReference().Degree(),
    latitude,
    DOUBLE_TOLERANCE);
  EXPECT_NEAR(sc.LongitudeReference().Degree(),
    longitude,
    DOUBLE_TOLERANCE);
}



//////////////////////////////////////////////////
int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
