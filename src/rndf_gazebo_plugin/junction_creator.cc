/*
 * Copyright (C) 2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/
#include "junction_creator.hh"

namespace gazebo {

LaneTermination::LaneTermination() {
}

LaneTermination::LaneTermination(
  const manifold::rndf::UniqueId &_id,
  const ignition::math::Pose3d &_pose,
  const double _laneWidth) {
  this->id = _id;
  this->pose = _pose;
  if (_laneWidth <= 0.0) {
    this->laneWidth = Creator::DEFAULT_LANE_WIDTH;
  }
  else
    this->laneWidth = _laneWidth;
}

LaneTermination::~LaneTermination() {
}

manifold::rndf::UniqueId& LaneTermination::Id() {
  return id;
}

ignition::math::Pose3d& LaneTermination::Pose() {
  return pose;
}

double& LaneTermination::LaneWidth() {
  return laneWidth;
}

std::vector<ignition::math::Vector3d> LaneTermination::ExtentPositions() {
  ignition::math::Vector3d a1, a2;
  ComputeExtents(pose.Pos(),
    pose.Rot().Yaw() + M_PI/2.0,
    laneWidth,
    a1,
    a2);
  return std::vector<ignition::math::Vector3d>{a1, a2};
}

Junction::Junction() {
}

Junction::Junction(
  std::vector<gazebo::LaneTermination>& _laneTerminations) {
  this->laneTerminations = _laneTerminations;
}

Junction::~Junction() {
}

std::vector<gazebo::LaneTermination>& Junction::LaneTerminations() {
  return laneTerminations;
}

void Junction::Compute() {
  for (uint i = 0; i < laneTerminations.size(); i++) {
    std::vector<ignition::math::Vector3d> const &extents =
      laneTerminations[i].ExtentPositions();
    points.push_back(extents[0]);
    points.push_back(extents[1]);
  }
  ignition::math::Vector3d &center = pose.Pos();
  center = gazebo::GetCenter(points);
  pose.Rot() = ignition::math::Quaterniond(0.0, 0.0, 0.0);
  std::sort(points.begin(),
    points.end(),
    gazebo::PolarSort(center));
  points.push_back(points.front());
}

void Junction::PrintPoints() {
  std::cout << ",," << std::endl;
  for (uint i = 0; i < points.size(); i++) {
    std::cout << points[i].X() << "," << points[i].Y() << std::endl;
  }
}

ignition::math::Pose3d& Junction::Pose() {
  return pose;
}

std::vector<ignition::math::Vector3d>& Junction::Points() {
  return points;
}

bool Junction::ContainsAny(std::vector<manifold::rndf::UniqueId> &ids) {
  std::vector<manifold::rndf::UniqueId> junctionIds;
  for (uint i = 0; i < laneTerminations.size(); i++) {
    junctionIds.push_back(laneTerminations[i].Id());
  }
  for (uint i = 0; i < ids.size(); i++) {
    if (std::find_if(junctionIds.begin(),
          junctionIds.end(),
          gazebo::LaneTermination::IdMatch(ids[i])) !=
        junctionIds.end()) {
      return true;
    }
  }
  return false;
}

int Junction::GetJunctionIndexById(
  std::vector<gazebo::Junction> &junctions,
  std::vector<gazebo::LaneTermination> &lts) {
  std::vector<manifold::rndf::UniqueId> ids;
  std::for_each(lts.begin(), lts.end(),
    [&](gazebo::LaneTermination &lt) {
      ids.push_back(lt.Id());
    });
  // Check if there is a match by ids
  for (uint i = 0; i < junctions.size(); i++) {
    gazebo::Junction &junction = junctions[i];
    if (junction.ContainsAny(ids))
      return static_cast<int>(i);
  }
  return -1;
}

JunctionCreator::JunctionCreator() : Creator() {
}

JunctionCreator::~JunctionCreator() {
}

void JunctionCreator::Junction(const gazebo::Junction &_junction) {
  this->junction = _junction;
}
void JunctionCreator::Material(const std::string &_material) {
  this->material = _material;
}
bool JunctionCreator::Create() {
  junction.Compute();
  ignition::msgs::Marker markerMsg;
  markerMsg.set_id(Creator::GetMarkerId());
  markerMsg.set_action(ignition::msgs::Marker::ADD_MODIFY);
  markerMsg.set_type(ignition::msgs::Marker::TRIANGLE_FAN);
  ignition::msgs::Material *matMsg = markerMsg.mutable_material();
  matMsg->mutable_script()->set_name(material);

  ignition::math::Vector3d &center = junction.Pose().Pos();
  center.Z() = Creator::DEFAULT_JUNCTIONS_HEIGHT;
  std::vector<ignition::math::Vector3d> &points =
    junction.Points();
  ignition::msgs::Set(markerMsg.add_point(), center);
  for (uint i = 0; i < points.size() ; i++) {
    ignition::msgs::Set(markerMsg.add_point(),
      ignition::math::Vector3d(points[i].X(),
        points[i].Y(),
        Creator::DEFAULT_JUNCTIONS_HEIGHT));
  }

  std::function<void(const ignition::msgs::StringMsg &,
    const bool)> functor = CreateHandlerFunctor();
  return nodePtr->Request(gazebo::Creator::MARKER_TOPIC,
    markerMsg, functor);
}

}
