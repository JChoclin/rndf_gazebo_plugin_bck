/*
 * Copyright (C) 2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

#include "creator.hh"

namespace gazebo {

const double Creator::DEFAULT_HEIGHT = 0.1;

const double Creator::DEFAULT_JUNCTIONS_HEIGHT = 2.0 * Creator::DEFAULT_HEIGHT;

const double Creator::DEFAULT_LANE_WIDTH = 3.6576;

const double Creator::DEFAULT_WAYPOINT_HEIGHT = 0.3;

const double Creator::DEFAULT_WAYPOINT_RADIUS = 1.0;

const double Creator::DEFAULT_INTERPOLATION_DISTANCE = 50.0;

const std::string Creator::MARKER_TOPIC = "/marker";

const std::string Creator::WAYPOINT_DEFAULT_MATERIAL = "Gazebo/White";

const std::string Creator::TEXT_MATERIAL = "Gazebo/White";

const double Creator::WAYPOINT_TEXT_Z_OFFSET = 4.0;

const double Creator::LANE_TEXT_Z_OFFSET = 8.0;

const uint Creator::NAME_INTERPOLATION_STEPS = 15;

const double Creator::WAYPOINT_NAME_SCALE = 2.0;

const double Creator::LANE_NAME_SCALE = 3.0;

const std::string Creator::LANE_DEFAULT_MATERIAL = "Gazebo/Black";

const std::string Creator::JUNCTION_DEFAULT_MATERIAL = "Gazebo/Residential";

const std::string Creator::PERIMETER_DEFAULT_MATERIAL = "Gazebo/Blue";

uint64_t Creator::markerId = 0;

uint64_t Creator::GetMarkerId() {
  return markerId++;
}

Creator::Creator() {
  nodePtr.reset(new ignition::transport::Node);
}

Creator::~Creator() {
  nodePtr.reset();
}

std::function<void(const ignition::msgs::StringMsg &,
const bool)> Creator::CreateHandlerFunctor(const std::string info) {
  std::shared_ptr<gazebo::Creator::Handler> handlerPtr;
  handlerPtr.reset(new gazebo::Creator::Handler);
  handlerPtr->Info(info);
  std::function<void(const ignition::msgs::StringMsg &,
    const bool)> functor;
  functor = std::bind(
    &Creator::Handler::operator(),
    handlerPtr, std::placeholders::_1, std::placeholders::_2);
  return functor;
}

}
