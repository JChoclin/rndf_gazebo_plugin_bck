/*
 * Copyright (C) 2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/
#include "perimeter_creator.hh"

namespace gazebo {

PerimeterCreator::PerimeterCreator() : Creator() {
  nodePtr.reset(new ignition::transport::Node);
}

PerimeterCreator::~PerimeterCreator() {
  nodePtr.reset();
}

bool PerimeterCreator::Create() {
  if (verteces.size() < 3)
    return false;
  ignition::msgs::Marker markerMsg;
  markerMsg.set_id(Creator::GetMarkerId());
  markerMsg.set_action(ignition::msgs::Marker::ADD_MODIFY);
  markerMsg.set_type(ignition::msgs::Marker::TRIANGLE_FAN);
  ignition::msgs::Material *matMsg = markerMsg.mutable_material();
  matMsg->mutable_script()->set_name(material);
  // Get the center of the perimeter
  ignition::math::Vector3d center(0.0, 0.0, 0.0);
  for (uint i = 0; i < verteces.size(); i++) {
    center = center + verteces[i];
  }
  center = center / static_cast<double>(verteces.size());
  center.Z() = Creator::DEFAULT_HEIGHT;
  // Load the verteces
  ignition::msgs::Set(markerMsg.add_point(), center);
  for (int i = verteces.size()-1; i >= 0 ; i--) {
    ignition::msgs::Set(markerMsg.add_point(),
      ignition::math::Vector3d(verteces[i].X(),
        verteces[i].Y(),
        Creator::DEFAULT_HEIGHT));
  }
  ignition::msgs::Set(markerMsg.add_point(),
      ignition::math::Vector3d(verteces[verteces.size()-1].X(),
        verteces[verteces.size()-1].Y(),
        Creator::DEFAULT_HEIGHT));

  std::function<void(const ignition::msgs::StringMsg &,
    const bool)> functor = CreateHandlerFunctor();
  return nodePtr->Request(gazebo::Creator::MARKER_TOPIC,
    markerMsg, functor);
}


void PerimeterCreator::Material(const std::string &_material) {
  this->material = _material;
}
void PerimeterCreator::Verteces(
  std::vector<ignition::math::Vector3d> &_verteces) {
    this->verteces = _verteces;
}

}
