/*
 * Copyright (C) 2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

#include <gazebo/gazebo.hh>
#include <ignition/math.hh>
#include "projection.hh"

#include "gtest/gtest.h"

#define DOUBLE_TOLERANCE  0.00001

TEST(PROJECTION_Test, SphericalToGlobal)
{
  gazebo::Projection projection;
  ignition::math::Vector3d sc, gc;
  sc = ignition::math::Vector3d(0.0, 0.0, 0.0);
  gc = projection.SphericalToGlobal(sc);
  EXPECT_NEAR(gc.Distance(
    ignition::math::Vector3d(0.0, 0.0, 0.0)),
    0.0,
    DOUBLE_TOLERANCE);
}

TEST(PROJECTION_Test, GetRNDFSpaceLimits)
{
  std::vector<ignition::math::Vector3d> locations;
  locations.push_back(
    ignition::math::Vector3d(-0.6036015095, -1.0200822381, 0.0));
  locations.push_back(
    ignition::math::Vector3d(-0.6035802339, -1.0200714869, 0.0));
  locations.push_back(
    ignition::math::Vector3d(-0.6035919974, -1.0201029901, 0.0));
  locations.push_back(
    ignition::math::Vector3d(-0.6035707917, -1.0200937747, 0.0));

  gazebo::Projection projection;
  projection.GetRNDFSpaceLimits(locations);
  ignition::math::SphericalCoordinates center =
    projection.GetCenterOfThePlane();

  EXPECT_NEAR(center.LatitudeReference().Radian(),
    -0.6035861331,
    DOUBLE_TOLERANCE);
  EXPECT_NEAR(center.LongitudeReference().Radian(),
    -1.0200876225,
    DOUBLE_TOLERANCE);
  EXPECT_NEAR(center.ElevationReference(),
    0.0,
    DOUBLE_TOLERANCE);
}


//////////////////////////////////////////////////
int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
