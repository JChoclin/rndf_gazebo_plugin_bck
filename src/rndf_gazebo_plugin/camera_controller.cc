/*
 * Copyright (C) 2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

#include "camera_controller.hh"

namespace gazebo {

CameraController::CameraController() {
}

CameraController::~CameraController() {
}

void CameraController::SetExtents(ignition::math::Vector3d const &min,
  ignition::math::Vector3d const &max) {
    this->x_min = min.X();
    this->x_max = max.X();
    this->y_max = min.Y();
    this->y_min = max.Y();
    this->z_min = min.Z();
    this->z_max = max.Z();
}

void CameraController::AdjustCamera() {
  UpdateCameraPtr();
  // Get horizontal and vertical angle to get the vertical pose
  // from the bigger of both
  double angle = camera->HFOV().Radian();
  // Get the extents of the surface to covegir
  double x = std::abs(x_max - x_min);
  double y = std::abs(y_max - y_min);
  // Get both z axis projections and get the mean from them
  double zy = 0.5 * y / std::atan(0.5 * angle);
  double zx = 0.5 * x / std::atan(0.5 * angle);
  double z = std::max(zy, zx);
  // Load the pose
  ignition::math::Pose3d pose;
  pose.Pos().X() = 0.5 * (x_max + x_min);
  pose.Pos().Y() = 0.5 * (y_max + y_min);
  pose.Pos().Z() = 0.5 * (z_max + z_min) + z;
  pose.Rot().Euler(0.0, ignition::math::Angle::HalfPi.Radian(), 0.0);
  camera->SetWorldPose(pose);
}
void CameraController::AdjustCamera(const ignition::math::Vector3d &axis) {
  UpdateCameraPtr();
  // Load the pose
  ignition::math::Pose3d pose;
  pose.Pos().X() = 0.5 * (x_max + x_min);
  pose.Pos().Y() = 0.5 * (y_max + y_min);
  pose.Pos().Z() = 0.5 * (z_max + z_min);
  pose.Rot().Axis(axis, 0.0);
  camera->SetWorldPose(pose);
}

void CameraController::SetToDefaultPose() {
  UpdateCameraPtr();
  gazebo::math::Pose cameraDefaultPose = camera->DefaultPose();
  camera->SetWorldPose(cameraDefaultPose.Ign());
}

ignition::math::Pose3d CameraController::GetCurrentPose() {
  UpdateCameraPtr();
  return camera->WorldPose();
}

void CameraController::UpdateCameraPtr() {
  camera = ::gazebo::gui::get_active_camera();
}

}
