/*
 * Copyright (C) 2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

#include "dynamic_render.hh"

namespace gazebo {

DynamicRender::DynamicRender() {
  printWaypoints = true;
  printLanes = true;
  printPerimeter = true;
  printJunctions = true;
  printPerimeter = true;
  originIsSet = false;
  printLabels = true;
  interpolationDistance = Creator::DEFAULT_INTERPOLATION_DISTANCE;
  waypointMaterial = Creator::WAYPOINT_DEFAULT_MATERIAL;
  laneMaterial = Creator::LANE_DEFAULT_MATERIAL;
  junctionMaterial = Creator::JUNCTION_DEFAULT_MATERIAL;
  perimeterMaterial = Creator::PERIMETER_DEFAULT_MATERIAL;
}

DynamicRender::~DynamicRender() {
  roadNetwork.reset();
  rndfInfo.reset();
  node->Fini();
  node.reset();
}

void DynamicRender::Load(gazebo::physics::WorldPtr _parent,
  sdf::ElementPtr _sdf) {
  worldPtr = _parent;

  ParseSDF(_sdf);
  LoadRNDFFile();
  PrintRNDFStats();

  std::vector<ignition::math::Vector3d> positions;
  GetAllWaypointLocations(positions);

  if (originIsSet)
    projection.GetRNDFSpaceLimits(positions, origin);
  else
    projection.GetRNDFSpaceLimits(positions);

  // Draw the lanes and waypoints and adjust the camera
  LoadZones(rndfInfo->Zones());
  LoadSegments(rndfInfo->Segments());
  cameraController.SetExtents(projection.GetMin(),
    projection.GetMax());

  // Subscribe to waypoints request messages, from where it receives
  // requests for paths
  node = transport::NodePtr(new transport::Node());
  this->node->Init();
  const std::string topicName = "~/road_waypoints_requests";
  this->waypointsRequestsSubscriber = this->node->Subscribe(
            topicName, &DynamicRender::OnWaypointsRequestsMsgs, this);
}


void DynamicRender::ParseSDF(sdf::ElementPtr _sdfPtr) {
  // This code gets the plugin node from the sdf file and
  // searches for different properties inside it that are
  // interesting for the plugin. In case there is no world or
  // plugin node an exception is raised.
  // 'rnfd' contains the path to the RNDF file. In case we cannot
  // find 'rndf' tag, an exception is raised.
  // 'lanes' contains a boolean indicating if we should draw them
  // 'waypoints' contains a boolean indicating if we should draw
  // them
  sdfPtr = _sdfPtr;
  if (sdfPtr->HasElement("rndf")) {
    filePath = sdfPtr->Get<std::string>("rndf");
  }
  else
    gzthrow("There is no rndf tag inside rndf node.");
  if (sdfPtr->HasElement("lanes")) {
    printLanes = sdfPtr->Get<bool>("lanes");
  }
  if (sdfPtr->HasElement("waypoints")) {
    printWaypoints = sdfPtr->Get<bool>("waypoints");
  }
  if (sdfPtr->HasElement("perimeter")) {
    printPerimeter = sdfPtr->Get<bool>("perimeter");
  }
  if (sdfPtr->HasElement("junctions")) {
    printJunctions = sdfPtr->Get<bool>("junctions");
  }
  if (sdfPtr->HasElement("waypoints_material")) {
    waypointMaterial =
      sdfPtr->Get<std::string>("waypoints_material");
  }
  if (sdfPtr->HasElement("lane_material")) {
    laneMaterial =
      sdfPtr->Get<std::string>("lane_material");
  }
  if (sdfPtr->HasElement("junction_material")) {
    junctionMaterial =
      sdfPtr->Get<std::string>("junction_material");
  }
  if (sdfPtr->HasElement("perimeter_material")) {
    perimeterMaterial =
      sdfPtr->Get<std::string>("perimeter_material");
  }
  if (sdfPtr->HasElement("interpolation_distance")) {
    interpolationDistance =
      sdfPtr->Get<double>("interpolation_distance");
  }
  if (sdfPtr->HasElement("origin")) {
    gazebo::math::Vector3 _origin =
      sdfPtr->Get<gazebo::math::Vector3>("origin");
    origin = gazebo::GetSphericalCoordinates(_origin.x,
      _origin.y,
      _origin.z);
    originIsSet = true;
  }
  if (sdfPtr->HasElement("print_labels")) {
    printLabels = sdfPtr->Get<bool>("print_labels");
  }
}


void DynamicRender::LoadRNDFFile() {
  std::string rndfFilePath = gazebo::common::find_file(filePath);
  if (rndfFilePath.empty()) {
    gzthrow(std::string("File [") +
      filePath +
      std::string("] does not exist."));
  }
  rndfInfo.reset(new manifold::rndf::RNDF(rndfFilePath));
  if (!rndfInfo->Valid()) {
    gzthrow(std::string("File [") +
      rndfFilePath +
      std::string("] is invalid"));
  }

  // Create the road network
  roadNetwork.reset(new manifold::RoadNetwork(*rndfInfo));
}

void DynamicRender::PrintRNDFStats() {
  // Show stats.
  gzmsg << "Name:               [" << rndfInfo->Name() << "]" << std::endl;
  if (!rndfInfo->Version().empty()) {
    gzmsg << "Version:            ["
      << rndfInfo->Version() << "]"
      << std::endl;
  }
  if (!rndfInfo->Date().empty()) {
    gzmsg << "Creation date:      ["
      << rndfInfo->Date() << "]"
      << std::endl;
  }
  gzmsg << "Number of segments: " << rndfInfo->NumSegments() << std::endl;
  gzmsg << "Number of zones:    " << rndfInfo->NumZones() << std::endl;
}

void DynamicRender::LoadSegments(
  std::vector<manifold::rndf::Segment> &segments) {
    if (printLanes) {
      for (uint i = 0; i < segments.size(); i++) {
        manifold::rndf::Segment &segment = segments[i];
        LoadLanes(segment.Id(), segment.Lanes());
      }
    }
    if (printJunctions) {
      LoadJunctions();
    }
    if (printWaypoints) {
      LoadWaypoints();
    }
}

void DynamicRender::LoadLanes(const int segmentParent,
  std::vector<manifold::rndf::Lane> &lanes) {
  roadCreator.DisplayNames(printLabels);
  roadCreator.Material(laneMaterial);

  for (const auto &lane : lanes) {
    // Get the name of the lane
    std::string laneName = CreateLaneName(segmentParent, lane.Id());
    std::string laneUniqueName = std::to_string(segmentParent) + "."
      + std::to_string(lane.Id());
    std::vector<ignition::math::Vector3d> roadPoints;
    FillLanePoints(laneUniqueName, lane, roadPoints);
    // Create a the lanes in gazebo
    roadCreator.Width(lane.Width());
    roadCreator.Points(roadPoints);
    roadCreator.Name(laneName);
    roadCreator.Create();
  }
}

void DynamicRender::FillLaneWaypointPositions(
  const std::string &laneName,
  const manifold::rndf::Lane &lane,
  std::vector<ignition::math::Vector3d> &waypointPositions) {
  uint amountOfWaypoints = lane.Waypoints().size();
  for (uint i = 1; i <= amountOfWaypoints; i++) {
    auto vertexName = laneName + "." + std::to_string(i);
    auto vertex = roadNetwork->Graph().Vertexes(vertexName)[0];
    waypointPositions.push_back(
      GetPoseWithoutOrientationFromVertex(vertex).Pos());
  }
}

void DynamicRender::FillLanePoints(
  const std::string &laneName,
  const manifold::rndf::Lane &lane,
  std::vector<ignition::math::Vector3d> &lanePoints) {
  std::vector<ignition::math::Vector3d> waypointPositions;
  FillLaneWaypointPositions(laneName, lane, waypointPositions);
  auto roadPoints = roadCreator.InterpolateRoad(waypointPositions,
    interpolationDistance);
  lanePoints.clear();
  lanePoints.insert(lanePoints.end(), roadPoints.begin(), roadPoints.end());
}

void DynamicRender::LoadWaypoints() {
  // Get all the vertices
  // Get the pose of each vertex going through the possible
  // adjacenct edges that belong to the same road.
  waypointCreator.Radius(Creator::DEFAULT_WAYPOINT_RADIUS);
  waypointCreator.Material(waypointMaterial);
  waypointCreator.DisplayNames(printLabels);

  auto vertices = roadNetwork->Graph().Vertexes();
  for (auto vertex : vertices) {
    waypointCreator.Name(vertex->Name());
    waypointCreator.SetType(GetTypeFromVertex(vertex));
    waypointCreator.Pose(GetPoseFromVertex(vertex));
    waypointCreator.Create();
  }
}

gazebo::WayPointCreator::Type DynamicRender::GetTypeFromVertex(
  std::shared_ptr<ignition::math::Vertex<std::string>> vertex) {
  manifold::rndf::UniqueId id(vertex->Name());
  manifold::rndf::RNDFNode *rndfNode = rndfInfo->Info(id);
  if (rndfNode != nullptr) {
    if (rndfNode->Lane() != nullptr) {
      return gazebo::WayPointCreator::Type::DEFAULT;
    }
    else if (rndfNode->Zone() != nullptr)
    {
      if (id.Y() == 0) {
        return gazebo::WayPointCreator::Type::PERIMETER;
      }
      else
      {
        return gazebo::WayPointCreator::Type::SPOT;
      }
    }
  }
  gzthrow("Waypoint not found");
}

ignition::math::Pose3d DynamicRender::GetPoseFromVertex(
  std::shared_ptr<ignition::math::Vertex<std::string>> vertex) {
  // Get the next vertex from the adjacent waypoints
  auto nextVertex = GetNextVertex(vertex);
  // Get the pose from the two vertices
  if (nextVertex != nullptr) {
    return GetPoseFromTwoVertices(vertex, nextVertex);
  }
  // It may be the last vertex in the road, so we try to get the
  // previous vertex pose and copy it
  auto previousVertex = GetPreviousVertex(vertex);
  if (previousVertex != nullptr) {
    return GetInverseFlowPoseFromTwoVertices(vertex, previousVertex);
  }
  // As we are just a waypoint, our pose should be just out position,
  // without a heading
  return GetPoseWithoutOrientationFromVertex(vertex);
}

std::shared_ptr<ignition::math::Vertex<std::string>>
  DynamicRender::GetNextVertex(
    std::shared_ptr<ignition::math::Vertex<std::string>> vertex) {
  manifold::rndf::UniqueId id(vertex->Name());
  auto vertices = roadNetwork->Graph().Adjacents(vertex);
  for (auto _vertex : vertices) {
    manifold::rndf::UniqueId _id(_vertex->Name());
    if (id.X() == _id.X() &&
      id.Y() == _id.Y() &&
      (id.Z() + 1) == _id.Z())
      return _vertex;
  }
  std::shared_ptr<ignition::math::Vertex<std::string>> nextVertex;
  nextVertex.reset();
  return nextVertex;
}

std::shared_ptr<ignition::math::Vertex<std::string>>
  DynamicRender::GetPreviousVertex(
    std::shared_ptr<ignition::math::Vertex<std::string>> vertex) {
  manifold::rndf::UniqueId id(vertex->Name());
  auto edges = roadNetwork->Graph().Incidents(vertex);
  for (auto edge : edges) {
    manifold::rndf::UniqueId _id(edge->Tail()->Name());
    if (id.X() == _id.X() &&
      id.Y() == _id.Y() &&
      (id.Z() - 1) == _id.Z())
      return edge->Tail();
  }
  std::shared_ptr<ignition::math::Vertex<std::string>> nextVertex;
  nextVertex.reset();
  return nextVertex;
}

ignition::math::Pose3d DynamicRender::GetPoseFromTwoVertices(
  std::shared_ptr<ignition::math::Vertex<std::string>> vertex,
  std::shared_ptr<ignition::math::Vertex<std::string>> nextVertex) {
  auto vertexPose = GetPoseWithoutOrientationFromVertex(vertex);
  auto nextVertexPose = GetPoseWithoutOrientationFromVertex(nextVertex);
  auto direction = nextVertexPose.Pos() - vertexPose.Pos();
  double yaw = std::atan2(direction.Y(), direction.X());
  vertexPose.Rot().Axis(0.0, 0.0, 1.0, yaw);
  return vertexPose;
}

ignition::math::Pose3d DynamicRender::GetInverseFlowPoseFromTwoVertices(
  std::shared_ptr<ignition::math::Vertex<std::string>> vertex,
  std::shared_ptr<ignition::math::Vertex<std::string>> nextVertex) {
  auto vertexPose = GetPoseWithoutOrientationFromVertex(vertex);
  auto nextVertexPose = GetPoseWithoutOrientationFromVertex(nextVertex);
  auto direction = vertexPose.Pos() - nextVertexPose.Pos();
  double yaw = std::atan2(direction.Y(), direction.X());
  vertexPose.Rot().Axis(0.0, 0.0, 1.0, yaw);
  return vertexPose;
}

ignition::math::Pose3d DynamicRender::GetPoseWithoutOrientationFromVertex(
  std::shared_ptr<ignition::math::Vertex<std::string>> vertex) {
  ignition::math::Pose3d pose;
  auto *waypoint = GetWaypointByUniqueId(
    manifold::rndf::UniqueId(vertex->Name()));
  if (waypoint == nullptr) {
    gzerr << "[DynamicRender::GetPoseWithoutOrientationFromVertex]: " <<
      "nullptr in waypoint --> " << vertex->Name() << std::endl;
    return pose;
  }
  auto location = GetWaypointSphericalLocation(*waypoint);
  pose.Pos() = projection.SphericalToGlobal(location);
  pose.Rot().Axis(0.0, 0.0, 0.1, 0.0);
  return pose;
}

void DynamicRender::LoadZones(
  std::vector<manifold::rndf::Zone> &zones) {
    for (auto &zone : zones) {
      LoadPerimeter(zone.Id(), zone.Perimeter());
    }
}

void DynamicRender::LoadPerimeter(const int zoneId,
  manifold::rndf::Perimeter &perimeter) {
  if (!printPerimeter)
    return;
  std::vector<ignition::math::Vector3d> vertices;
  FillPerimeterVertices(zoneId, perimeter, vertices);
  perimeterCreator.Material(perimeterMaterial);
  perimeterCreator.Verteces(vertices);
  perimeterCreator.Create();
}

void DynamicRender::FillPerimeterVertices(const int zoneId,
  const manifold::rndf::Perimeter &perimeter,
  std::vector<ignition::math::Vector3d> &vertices) {
  uint verticesSize = perimeter.Points().size();
  for (uint i = 1; i <= verticesSize; i++) {
    auto vertexName = std::to_string(zoneId) +
      "." + std::to_string(0) + "." + std::to_string(i);
    auto vertex = roadNetwork->Graph().Vertexes(vertexName)[0];
    vertices.push_back(
      GetPoseWithoutOrientationFromVertex(vertex).Pos());
  }
}

void DynamicRender::LoadJunctions() {
  std::vector<gazebo::Junction> junctions;
  GenerateJunctions(junctions);
  junctionCreator.Material("Gazebo/Residential");
  for (uint i = 0; i < junctions.size(); i++) {
    junctionCreator.Junction(junctions[i]);
    junctionCreator.Create();
  }
}

manifold::rndf::Waypoint* DynamicRender::GetWaypointByUniqueId(
  const manifold::rndf::UniqueId &waypointId) {
  manifold::rndf::RNDFNode *rndfNode = rndfInfo->Info(waypointId);
  if (rndfNode != nullptr) {
    if (rndfNode->Lane() != nullptr) {
      for (auto &waypoint : rndfNode->Lane()->Waypoints()) {
        if (waypoint.Id() == waypointId.Z()) {
          return &waypoint;
        }
      }
    }
    else if (rndfNode->Zone() != nullptr)
    {
      if (waypointId.Y() == 0) {
        // This is a perimeter node
        for (auto &waypoint : rndfNode->Zone()->Perimeter().Points()) {
          if (waypoint.Id() == waypointId.Z()) {
            return &waypoint;
          }
        }
      }
      else
      {
        for (auto &parkingSpot : rndfNode->Zone()->Spots()) {
          if (parkingSpot.Id() == waypointId.Y()) {
            for (auto &waypoint : parkingSpot.Waypoints()) {
              if (waypoint.Id() == waypointId.Z()) {
                return &waypoint;
              }
            }
          }
        }
      }
    }
  }
  return nullptr;
}

void DynamicRender::OnWaypointsRequestsMsgs(WaypointsRequestPtr &_msg) {
    rndf_gazebo_plugin_msgs::msgs::WaypointsResponse response_msg;

    response_msg.set_client_name(_msg->client_name());

    gzmsg << "Received path request from " <<
      _msg->client_name();
    gzmsg << " Waypoint requested " <<
      _msg->initial_waypoint().x();
    gzmsg <<  _msg->initial_waypoint().y() << " " <<
      _msg->initial_waypoint().z() << std::endl;

    manifold::rndf::Lane *lane;
    const manifold::rndf::UniqueId wpId(
        _msg->initial_waypoint().x(),
        _msg->initial_waypoint().y(),
        _msg->initial_waypoint().z());
    lane = GetLaneByUniqueId(wpId);
    if (lane == nullptr) {
      gzerr << "Error. Lane " << CreateLaneName(wpId.X(), wpId.Y())
        << " has not been found.";
      return;
    }

    std::string laneUniqueName = std::to_string(wpId.X()) +
      "." + std::to_string(wpId.Y());
    std::vector<ignition::math::Vector3d> lanePoints;
    FillLanePoints(laneUniqueName, *lane, lanePoints);
    // Get all the waypoints positions and interpolated
    // points positions of the lane
    for (auto point : lanePoints) {
      auto *_point = response_msg.add_lane_points_positions();
      _point->set_x(point.X());
      _point->set_y(point.Y());
      _point->set_z(point.Z());
    }

    std::vector<ignition::math::Vector3d> waypointPositions;
    FillLaneWaypointPositions(laneUniqueName, *lane, waypointPositions);
    for (uint i = 0; i < waypointPositions.size(); i++) {
      auto* waypointId = response_msg.add_waypoints_ids();
      waypointId->set_x(_msg->initial_waypoint().x());
      waypointId->set_y(_msg->initial_waypoint().y());
      waypointId->set_z(i+1);

      auto *waypointPositionPtr = response_msg.add_waypoints_positions();
      waypointPositionPtr->set_x(waypointPositions[i].X());
      waypointPositionPtr->set_y(waypointPositions[i].Y());
      waypointPositionPtr->set_z(waypointPositions[i].Z());
    }

    // Get the exits and entries of the lane, then create vectors
    // to keep the positions of each one
    std::vector<manifold::rndf::Exit> &exits = lane->Exits();
    for (auto &exit : exits) {
      // Get next waypoint to go from this exit,
      // which it is the entry associated to this exit
      manifold::rndf::Waypoint *waypointPointer =
        GetWaypointByUniqueId(exit.EntryId());
      if (waypointPointer == nullptr)
        return;
      // Get the position of this entry and save it into the msg
      ignition::math::Vector3d entryLocation =
        GetWaypointSphericalLocation(*waypointPointer);
      ignition::math::Vector3d entryPosition =
        projection.SphericalToGlobal(entryLocation);
      gazebo::msgs::Vector3d* entryPositionPtr =
        response_msg.add_next_entries_positions();
      entryPositionPtr->set_x(entryPosition[0]);
      entryPositionPtr->set_y(entryPosition[1]);
      entryPositionPtr->set_z(entryPosition[2]);
      // Save entry id into the message
      gazebo::msgs::Vector3d* entryIdPtr =
        response_msg.add_next_entries_ids();
      entryIdPtr->set_x(exit.EntryId().X());
      entryIdPtr->set_y(exit.EntryId().Y());
      entryIdPtr->set_z(exit.EntryId().Z());
      // Save exit ids
      gazebo::msgs::Vector3d* exitIdPtr =
        response_msg.add_exit_ids();
      exitIdPtr->set_x(exit.ExitId().X());
      exitIdPtr->set_y(exit.ExitId().Y());
      exitIdPtr->set_z(exit.ExitId().Z());
    }
    // Publish msg
    std::string topicName = std::string("~/") +
      _msg->client_name()  + "/rndf_path";
    gazebo::transport::PublisherPtr responsePub =
      node->Advertise<
        rndf_gazebo_plugin_msgs::msgs::WaypointsResponse>(topicName);

    responsePub->Publish(response_msg);
}

manifold::rndf::Lane* DynamicRender::GetLaneByUniqueId(
  const manifold::rndf::UniqueId &waypointId) {
  manifold::rndf::RNDFNode *rndfNode = rndfInfo->Info(waypointId);
  if (rndfNode == nullptr)
    return nullptr;
  return rndfNode->Lane();
}


std::string DynamicRender::CreateLaneName(const int segmentParentId,
  const int laneId) {
  return "lane_" + std::to_string(segmentParentId) +
    "_" + std::to_string(laneId);
}

void DynamicRender::GenerateJunctions(
  std::vector<gazebo::Junction> &junctions) {
  auto edges = roadNetwork->Graph().Edges();
  // Create the exits pairs
  std::vector<manifold::rndf::Exit> exits;
  for (auto &edge : edges) {
    manifold::rndf::UniqueId tail =
      manifold::rndf::UniqueId(edge->Tail()->Name());
    manifold::rndf::UniqueId head =
      manifold::rndf::UniqueId(edge->Head()->Name());
    if (head.X() == tail.X()) {
      continue;
    }
    exits.push_back(manifold::rndf::Exit(tail, head));
  }
  // Fills the junctions
  FillJunctions(exits, junctions);
}

void DynamicRender::FillJunctions(
  std::vector<manifold::rndf::Exit> const &exits,
  std::vector<gazebo::Junction> &junctions) {
    for (uint j = 0; j < exits.size(); j++) {
      manifold::rndf::Exit const &exit = exits[j];

      gazebo::LaneTermination const &ltExit =
        CreateLaneTermination(exit.ExitId());
      gazebo::LaneTermination const &ltEntry =
        CreateLaneTermination(exit.EntryId());
      std::vector<gazebo::LaneTermination> lts;
      lts.push_back(ltEntry);
      lts.push_back(ltExit);

      int junctionId = gazebo::Junction::GetJunctionIndexById(
        junctions, lts);

      if (junctionId == -1) {
        gazebo::Junction junction;
        junction.LaneTerminations().push_back(ltExit);
        junction.LaneTerminations().push_back(ltEntry);
        junctions.push_back(junction);
      }
      else
      {
        junctions[junctionId].LaneTerminations().push_back(ltExit);
        junctions[junctionId].LaneTerminations().push_back(ltEntry);
      }
    }
}

gazebo::LaneTermination DynamicRender::CreateLaneTermination(
  const manifold::rndf::UniqueId &id) {
    auto vertices = roadNetwork->Graph().Vertexes(id.String());
    ignition::math::Pose3d pose = GetPoseFromVertex(vertices[0]);

    double width;
    manifold::rndf::Lane *lane = GetLaneByUniqueId(id);
    if (lane == nullptr)
      width = Creator::DEFAULT_LANE_WIDTH;
    else
      width = lane->Width();

    return gazebo::LaneTermination(id, pose, width);
}

ignition::math::Vector3d
  DynamicRender::GetWaypointSphericalLocation(
    manifold::rndf::Waypoint &wp) {
  ignition::math::SphericalCoordinates &location =
    wp.Location();
  return ignition::math::Vector3d(
    location.LatitudeReference().Radian(),
    location.LongitudeReference().Radian(),
    location.ElevationReference());
}

void DynamicRender::GetAllWaypointLocations(
  std::vector<ignition::math::Vector3d> &positions) {
  auto vertices = roadNetwork->Graph().Vertexes();
  for (auto vertex : vertices) {
    auto *wp = GetWaypointByUniqueId(
      manifold::rndf::UniqueId(vertex->Name()));
    positions.push_back(GetWaypointSphericalLocation(*wp));
  }
}

}
