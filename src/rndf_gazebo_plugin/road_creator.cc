/*
 * Copyright (C) 2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/
#include "road_creator.hh"

namespace gazebo {

RoadCreator::RoadCreator() : Creator(),
  printLabels(true) {
  textCreator.Material(Creator::TEXT_MATERIAL);
  textCreator.Scale(ignition::math::Vector3d(
    Creator::LANE_NAME_SCALE,
    Creator::LANE_NAME_SCALE,
    Creator::LANE_NAME_SCALE));
}

RoadCreator::~RoadCreator() {
}

std::vector<ignition::math::Vector3d> RoadCreator::InterpolateRoad(
  const std::vector<ignition::math::Vector3d> &_points,
  const double distanceThreshold) {
  ignition::math::Spline spline;
  spline.AutoCalculate(true);
  std::vector<ignition::math::Vector3d> newPoints;

  // Add all the control points
  for (uint i = 0; i < _points.size(); i++) {
    spline.AddPoint(_points[i]);
  }

  double distance;
  for (uint i = 0; i < (_points.size()-1); i++) {
    newPoints.push_back(_points[i]);
    distance = _points[i].Distance(_points[i+1]);
    if (distance > distanceThreshold) {
      ignition::math::Vector3d newPoint = spline.Interpolate(i, 0.5);
      newPoints.push_back(newPoint);
    }
  }
  newPoints.push_back(_points.back());

  bool distanceCheck = true;
  for (uint i = 0; i < newPoints.size()-1; i++) {
    distance = newPoints[i].Distance(newPoints[i+1]);
    if (distance > distanceThreshold) {
      distanceCheck = false;
      break;
    }
  }
  if (distanceCheck == false) {
    return InterpolateRoad(newPoints, distanceThreshold);
  }

  return newPoints;
}

void RoadCreator::Width(const double _width) {
  if (_width <= 0.0)
    this->width = Creator::DEFAULT_LANE_WIDTH;
  else
    this->width = _width;
}
void RoadCreator::Name(const std::string &_name) {
  this->name = _name;
}
void RoadCreator::Points(std::vector<ignition::math::Vector3d> &_points) {
  this->points = _points;
}
void RoadCreator::Material(const std::string &_material) {
  this->material = _material;
}

bool RoadCreator::Create() {
  std::vector<ignition::math::Vector3d> laneExtents;
  std::vector<ignition::math::Pose3d> poses;

  FillPoses(points, poses);
  FillTriangleStripPoints(width, poses, laneExtents);

  if (printLabels)
    CreateNames(poses);

  ignition::msgs::Marker markerMsg;
  markerMsg.set_id(Creator::GetMarkerId());
  markerMsg.set_action(ignition::msgs::Marker::ADD_MODIFY);
  markerMsg.set_ns(name);
  markerMsg.set_type(ignition::msgs::Marker::TRIANGLE_LIST);
  ignition::msgs::Material *matMsg = markerMsg.mutable_material();
  matMsg->mutable_script()->set_name(material);

  std::for_each(laneExtents.begin(),
    laneExtents.end(),
    [&](ignition::math::Vector3d &extent) {
      extent.Z() = Creator::DEFAULT_HEIGHT;
      ignition::msgs::Set(markerMsg.add_point(),
        extent);
    });

  std::function<void(const ignition::msgs::StringMsg &,
      const bool)> functor = CreateHandlerFunctor(name);
  return nodePtr->Request(gazebo::Creator::MARKER_TOPIC,
    markerMsg, functor);
  return true;
}

void RoadCreator::CreateNames(
  const std::vector<ignition::math::Pose3d> &poses) {
  for (uint i = 0; i < poses.size(); i += Creator::NAME_INTERPOLATION_STEPS) {
    CreateName(poses[i]);
  }
  uint mod = poses.size() % Creator::NAME_INTERPOLATION_STEPS;
  if (mod != 0 &&
    mod >= (Creator::NAME_INTERPOLATION_STEPS / 2)) {
    CreateName(poses[poses.size() - 1]);
  }
}

void RoadCreator::CreateName(const ignition::math::Pose3d &pose) {
  textCreator.Name(name + "_text");
  textCreator.Text(name);
  ignition::math::Pose3d _pose(pose);
  _pose.Pos().Z() = _pose.Pos().Z() + Creator::LANE_TEXT_Z_OFFSET;
  _pose.Rot() = ignition::math::Quaterniond(0.0, 0.0, 0.0);
  textCreator.Pose(_pose);
  textCreator.Create();
}

void RoadCreator::FillPoses(
  const std::vector<ignition::math::Vector3d> &_points,
  std::vector<ignition::math::Pose3d> &poses) {
  if (_points.size() < 2)
    gzthrow("At least two points are needed to work. But, " +
      std::to_string(poses.size()) + " were given.");

  for (uint i = 0; i < (_points.size()-1); i++) {
    ignition::math::Vector3d direction =
      _points[i + 1] - _points[i];
    // Calculate Euler angles and create quaterion
    double yaw, roll, pitch;
    yaw = std::atan2(direction.Y(), direction.X()) -
      ignition::math::Angle::HalfPi.Radian();
    roll = std::atan2(direction.Z(), direction.Y());
    pitch = std::atan2(direction.Z(), direction.X());
    ignition::math::Quaterniond quaternion(roll, pitch, yaw);

    ignition::math::Pose3d pose;
    pose.Pos() = _points[i];
    pose.Rot() = quaternion;
    poses.push_back(pose);
  }
  ignition::math::Pose3d pose;
  pose.Pos() = _points.back();
  pose.Rot() = poses.back().Rot();
  poses.push_back(pose);
}

void RoadCreator::FillTriangleStripPoints(const double laneWidth,
  const std::vector<ignition::math::Pose3d> &poses,
  std::vector<ignition::math::Vector3d> &trianglePoints) {
  if (poses.size() < 2)
    gzthrow("At least two points are needed to work. But, " +
      std::to_string(poses.size()) + " were given.");
  for (uint i = 0; i < (poses.size() - 1); i++) {
    std::vector<ignition::math::Vector3d> verteces;
    verteces.push_back(ignition::math::Vector3d());
    verteces.push_back(ignition::math::Vector3d());
    verteces.push_back(ignition::math::Vector3d());
    verteces.push_back(ignition::math::Vector3d());

    ComputeExtents(poses[i].Pos(),
      poses[i].Rot().Yaw(),
      laneWidth,
      verteces[0],
      verteces[1]);
    ComputeExtents(poses[i + 1].Pos(),
      poses[i + 1].Rot().Yaw(),
      laneWidth,
      verteces[2],
      verteces[3]);

    gazebo::OrderPolar(verteces);
    ignition::math::Vector3d &a1 = verteces[0];
    ignition::math::Vector3d &a2 = verteces[1];
    ignition::math::Vector3d &b2 = verteces[2];
    ignition::math::Vector3d &b1 = verteces[3];

    std::vector<ignition::math::Vector3d> _points;
    _points.push_back(a1);
    _points.push_back(b2);
    _points.push_back(a2);
    gazebo::OrderPolar(_points);
    trianglePoints.insert(trianglePoints.end(),
      _points.begin(), _points.end());

    _points.clear();
    _points.push_back(a1);
    _points.push_back(b1);
    _points.push_back(b2);
    gazebo::OrderPolar(_points);
    trianglePoints.insert(trianglePoints.end(),
      _points.begin(), _points.end());
  }
}

void RoadCreator::DisplayNames(const bool _printLabels) {
  printLabels = _printLabels;
}

}

