/*
 * Copyright (C) 2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/
#include "text_creator.hh"

namespace gazebo {

TextCreator::TextCreator() {
  scale = ignition::math::Vector3d(1.0, 1.0, 1.0);
}

TextCreator::~TextCreator() {
}

void TextCreator::Name(const std::string &_name) {
  name = _name;
}

void TextCreator::Material(const std::string &_material) {
  material = _material;
}

void TextCreator::Text(const std::string &_text) {
  text = _text;
}

void TextCreator::Pose(const ignition::math::Pose3d &_pose) {
  pose = _pose;
}

void TextCreator::Scale(const ignition::math::Vector3d &_scale) {
  scale = _scale;
}

bool TextCreator::Create() {
  ignition::msgs::Marker markerMsg;
  markerMsg.set_id(Creator::GetMarkerId());
  markerMsg.set_action(ignition::msgs::Marker::ADD_MODIFY);
  markerMsg.set_ns(name);
  markerMsg.set_type(ignition::msgs::Marker::TEXT);
  ignition::msgs::Material *matMsg = markerMsg.mutable_material();
  matMsg->mutable_script()->set_name(material);
  markerMsg.set_text(text);
  ignition::msgs::Set(markerMsg.mutable_scale(),
    scale);
  ignition::msgs::Set(markerMsg.mutable_pose(),
    pose);

  std::function<void(const ignition::msgs::StringMsg &,
      const bool)> functor = CreateHandlerFunctor(name);
  return nodePtr->Request(gazebo::Creator::MARKER_TOPIC,
    markerMsg, functor);
}

}
