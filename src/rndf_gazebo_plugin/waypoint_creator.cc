/*
 * Copyright (C) 2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/
#include "waypoint_creator.hh"

namespace gazebo {


std::vector<ignition::math::Vector3d> WayPointCreator::arrowPoints(
  {
    ignition::math::Vector3d(2.0, 0.0, Creator::DEFAULT_WAYPOINT_HEIGHT),
    ignition::math::Vector3d(0.0, -2.0, Creator::DEFAULT_WAYPOINT_HEIGHT),
    ignition::math::Vector3d(0.0, 2.0, Creator::DEFAULT_WAYPOINT_HEIGHT),
    ignition::math::Vector3d(0.0, -1.0, Creator::DEFAULT_WAYPOINT_HEIGHT),
    ignition::math::Vector3d(-2.0, -1.0, Creator::DEFAULT_WAYPOINT_HEIGHT),
    ignition::math::Vector3d(-2.0, 1.0, Creator::DEFAULT_WAYPOINT_HEIGHT),
    ignition::math::Vector3d(0.0, 1.0, Creator::DEFAULT_WAYPOINT_HEIGHT),
    ignition::math::Vector3d(0.0, -1.0, Creator::DEFAULT_WAYPOINT_HEIGHT),
    ignition::math::Vector3d(-2.0, 1.0, Creator::DEFAULT_WAYPOINT_HEIGHT),
});

WayPointCreator::WayPointCreator() :
  Creator(),
  type(WayPointCreator::Type::DEFAULT),
  printLabels(true) {
  textCreator.Material(Creator::TEXT_MATERIAL);
  textCreator.Scale(ignition::math::Vector3d(
    Creator::WAYPOINT_NAME_SCALE,
    Creator::WAYPOINT_NAME_SCALE,
    Creator::WAYPOINT_NAME_SCALE));
}

WayPointCreator::~WayPointCreator() {
}

void WayPointCreator::Name(const std::string &_name) {
  this->name = _name;
}
void WayPointCreator::Pose(const ignition::math::Pose3d &_pose) {
  this->pose = _pose;
}
void WayPointCreator::Radius(const double _radius) {
  this->radius = _radius;
}
void WayPointCreator::Material(const std::string &_material) {
  this->material = _material;
}
void WayPointCreator::SetType(const WayPointCreator::Type _type) {
  this->type = _type;
}

bool WayPointCreator::Create() {
  if (printLabels)
    CreateName();

  ignition::msgs::Marker markerMsg;
  markerMsg.set_id(Creator::GetMarkerId());
  markerMsg.set_action(ignition::msgs::Marker::ADD_MODIFY);

  ignition::msgs::Material *matMsg = markerMsg.mutable_material();
  if (material.empty())
    matMsg->mutable_script()->set_name(Creator::WAYPOINT_DEFAULT_MATERIAL);
  else
    matMsg->mutable_script()->set_name(material);

  switch (type) {
    case WayPointCreator::Type::SPOT:
    case WayPointCreator::Type::PERIMETER:
    case WayPointCreator::Type::STOP:
      CreateCircleGeometry(markerMsg, radius);
    break;

    case WayPointCreator::Type::DEFAULT:
    case WayPointCreator::Type::CHECKPOINT:
    case WayPointCreator::Type::ENTRY:
    case WayPointCreator::Type::EXIT:
      CreateArrowGeometry(markerMsg, radius);
    break;

    default:
      return false;
    break;
  }

  std::function<void(const ignition::msgs::StringMsg &,
    const bool)> functor = CreateHandlerFunctor(name);
  return nodePtr->Request(gazebo::Creator::MARKER_TOPIC,
    markerMsg, functor);
}

void WayPointCreator::CreateName() {
  textCreator.Name(name + "_text");
  textCreator.Text(name);
  ignition::math::Pose3d _pose(pose);
  _pose.Pos().Z() = _pose.Pos().Z() + Creator::WAYPOINT_TEXT_Z_OFFSET;
  _pose.Rot() = ignition::math::Quaterniond(0.0, 0.0, 0.0);
  textCreator.Pose(_pose);
  textCreator.Create();
}

void WayPointCreator::CreateCircleGeometry(ignition::msgs::Marker &markerMsg,
  const double _radius) {
    markerMsg.set_type(ignition::msgs::Marker::TRIANGLE_FAN);
    const ignition::math::Vector3d &center = pose.Pos();

    // Set the center and then all the other points
    ignition::math::Vector3d circleCenter = center;
    circleCenter.Z() = Creator::DEFAULT_WAYPOINT_HEIGHT;
    ignition::msgs::Set(markerMsg.add_point(), circleCenter);

    for (double t = 0; t <= (2.0 * M_PI); t += (M_PI / 6.0)) {
      ignition::math::Vector3d point =
        ignition::math::Vector3d(_radius * cos(t),
          _radius * sin(t), 0.0);
      point = point + center;
      point.Z() = Creator::DEFAULT_WAYPOINT_HEIGHT;
      ignition::msgs::Set(markerMsg.add_point(), point);
    }
    ignition::math::Vector3d point =
      ignition::math::Vector3d(_radius * cos(0.0),
        _radius * sin(0.0), 0.0);
    point = point + center;
    point.Z() = Creator::DEFAULT_WAYPOINT_HEIGHT;
    ignition::msgs::Set(markerMsg.add_point(), point);
}

void WayPointCreator::CreateArrowGeometry(ignition::msgs::Marker &markerMsg,
  const double _radius) {
    markerMsg.set_type(ignition::msgs::Marker::TRIANGLE_LIST);

    const ignition::math::Vector3d &center = pose.Pos();
    std::vector<ignition::math::Vector3d> points;
    for (uint i = 0; i < arrowPoints.size(); i += 3) {
      ignition::math::Vector3d pointA, pointB, pointC;
      pointA =
        pose.Rot().RotateVector(arrowPoints[i]) * _radius +
        center;
      pointA.Z() = Creator::DEFAULT_WAYPOINT_HEIGHT;

      pointB =
        pose.Rot().RotateVector(arrowPoints[i+1]) * _radius +
        center;
      pointB.Z() = Creator::DEFAULT_WAYPOINT_HEIGHT;

      pointC =
        pose.Rot().RotateVector(arrowPoints[i+2]) * _radius +
        center;
      pointC.Z() = Creator::DEFAULT_WAYPOINT_HEIGHT;

      std::vector<ignition::math::Vector3d> triangle(
        {pointA, pointB, pointC});
      gazebo::OrderPolar(triangle);
      points.insert(points.end(), triangle.begin(), triangle.end());
    }
    for (uint i = 0; i < points.size(); i++) {
      ignition::msgs::Set(markerMsg.add_point(), points[i]);
    }
}

void WayPointCreator::DisplayNames(const bool _printLabels) {
  printLabels = _printLabels;
}

}
