/*
 * Copyright (C) 2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

#include <assert.h>
#include <algorithm>
#include <string>

#include "road_driver_plugin.hh"

namespace gazebo {
// Register this plugin with the simulator
GZ_REGISTER_MODEL_PLUGIN(RoadDriver)

/////////////////////////////////////////////////
RoadDriver::RoadDriver(): ModelPlugin() {
}

/////////////////////////////////////////////////
RoadDriver::~RoadDriver() {
}

/////////////////////////////////////////////////
void RoadDriver::Load(physics::ModelPtr _model, sdf::ElementPtr _sdf) {
  this->model = _model;
  this->world = model->GetWorld();

  this->node = transport::NodePtr(new transport::Node());
  this->node->Init();

  this->model_name = this->model->GetName();

  this->lastTime  = world->GetSimTime();
  this->startTime = this->lastTime;

  if (!_sdf->HasElement("linearVelocity")) {
    this->linearVelocity = 20;
    gzmsg << "[road_driver] Max. linear velocity not declared, using "
          << this->linearVelocity << std::endl;
  }
  else
  {
    this->linearVelocity = _sdf->Get<double>("linearVelocity");
    gzmsg << "[road_driver] Using linearVelocity: "
          << this->linearVelocity << std::endl;
  }

  if (!_sdf->HasElement("angularVelocity")) {
    this->angularVelocity = 4.0;
    gzmsg << "[road_driver] Max. angular velocity not declared, using "
          << this->angularVelocity << std::endl;
  }
  else
  {
    this->angularVelocity = _sdf->Get<double>("angularVelocity");
    gzmsg << "[road_driver] Using angularVelocity: "
          << this->linearVelocity << std::endl;
  }

  if (!_sdf->HasElement("updatePeriod")) {
    this->updatePeriod = 1.0/50.0;
    gzmsg << "[road_driver] updatePeriod not declared using: "
          << this->updatePeriod << std::endl;
  }
  else
  {
    this->updatePeriod = _sdf->Get<double>("updatePeriod");
    gzmsg << "[road_driver] Using updatePeriod: "
          << this->updatePeriod << std::endl;
  }

  if (!_sdf->HasElement("minimalDistance")) {
    this->minimalDistance = 2.0;
    gzmsg << "[road_driver] minimalDistance not declared using: "
          << this->minimalDistance << std::endl;
  }
  else
  {
    this->minimalDistance = _sdf->Get<double>("minimalDistance");
    gzmsg << "[road_driver] Using minimalDistance: "
          << this->minimalDistance << std::endl;
  }

  if (!_sdf->HasElement("initialWaitTime")) {
    this->initialWaitTime = 15.0;
    gzmsg << "[road_driver] initialWaitTime not declared using: "
          << this->initialWaitTime << std::endl;
  }
  else
  {
    this->initialWaitTime = _sdf->Get<double>("initialWaitTime");
    gzmsg << "[road_driver] Using minimalDistance: "
          << this->minimalDistance << std::endl;
  }

  // Configure starting point
  if (!_sdf->HasElement("initialSegment")) {
    this->initialLanePointID.x = 1;
  }
  else
  {
    this->initialLanePointID.x = _sdf->Get<int>("initialSegment");
  }
  if (!_sdf->HasElement("initialLane")) {
    this->initialLanePointID.y = 1;
  }
  else
  {
    this->initialLanePointID.y = _sdf->Get<int>("initialLane");
  }
  if (!_sdf->HasElement("initialLanePoint")) {
    this->initialLanePointID.z = 1;
  }
  else
  {
    this->initialLanePointID.z = _sdf->Get<int>("initialWaypoint");
  }
  gzmsg << "[road_driver] Initial lanePoint: " << this->initialLanePointID.x;
  gzmsg << " " << this->initialLanePointID.y
    << " " << this->initialLanePointID.z << std::endl;

  // Get PID values
  double axisXP;
  double axisYP;
  double axisZP;
  double axisRollP;
  double axisPitchP;
  double axisYawZP;

  if (_sdf->HasElement("axisXP"))
    axisXP = _sdf->Get<double>("axisXP");
  else
    axisXP = defaultP;
  if (_sdf->HasElement("axisYP"))
    axisYP = _sdf->Get<double>("axisiYP");
  else
    axisYP = defaultP;
  if (_sdf->HasElement("axisZP"))
    axisZP = _sdf->Get<double>("axisiZP");
  else
    axisZP = defaultP;
  if (_sdf->HasElement("axisRollP"))
    axisRollP = _sdf->Get<double>("axisiRollP");
  else
    axisRollP = defaultP;
  if (_sdf->HasElement("axisPitchP"))
    axisYawZP = _sdf->Get<double>("axisPitchP");
  else
    axisPitchP = defaultP;
  if (_sdf->HasElement("axisYawZP"))
    axisYawZP = _sdf->Get<double>("axisYawP");
  else
    axisYawZP = defaultP;

  // initialize the pid ( common::Pid ),
  this->axisXPID = common::PID(axisXP, defaultI, defaultD);
  this->axisYPID = common::PID(axisYP, defaultI, defaultD);
  this->axisZPID = common::PID(axisZP, defaultI, defaultD);
  this->axisRollPID = common::PID(axisRollP, defaultI, defaultD);
  this->axisPitchPID = common::PID(axisPitchP, defaultI, defaultD);
  this->axisYawZPID = common::PID(axisYawZP, defaultI, defaultD);

  this->updateConnection_ =
          event::Events::ConnectWorldUpdateBegin(
              boost::bind(&RoadDriver::OnUpdate, this, _1));

  // Create the lanePoints request publisher
  const std::string publisherTopic = "~/road_waypoints_requests";
  this->waypointsRequestPub = this->node->Advertise<
    rndf_gazebo_plugin_msgs::msgs::WaypointsRequest>(publisherTopic);

  this->waypointsRequestPub->WaitForConnection();

  // Create the lanePoints response subscriber
  const std::string subscriberTopic = std::string("~/") +
    this->model_name + "/rndf_path";
  this->pathSubscriber = this->node->Subscribe(
    subscriberTopic, &RoadDriver::OnWaipointsResponseMsg, this);
  gzmsg << "[road_driver] Starting Gazebo rndf Driver on topic: "
    << subscriberTopic << std::endl;

  this->waitResponse = false;
}

/////////////////////////////////////////////////
void RoadDriver::OnWaipointsResponseMsg(WaypointsResponsePtr &_msg) {
  this->mutex.lock();

  if (_msg->lane_points_positions().size() &&
    (waitResponse || firstLanePoint)) {
    math::Pose position = model->GetWorldPose();
    // Clear all registered entries and lanes from the previous lane
    this->pathLanePoints.clear();
    this->entriesIds.clear();
    this->entriesPositions.clear();
    this->waypointsIds.clear();
    this->waypointsPositions.clear();
    this->exitIds.clear();


    // Save next waypoints ids and positions
    for (int i = 0; i < _msg->waypoints_ids().size(); i++) {
        const math::Vector3 newWaypointId(
            _msg->waypoints_ids(i).x(),
            _msg->waypoints_ids(i).y(),
            _msg->waypoints_ids(i).z());
        this->waypointsIds.push_back(newWaypointId);
        const math::Vector3 newWaypointPosition(
            _msg->waypoints_positions(i).x(),
            _msg->waypoints_positions(i).y(),
            _msg->waypoints_positions(i).z());
        this->waypointsPositions.push_back(newWaypointPosition);
    }
    int j = -1;
    for (uint i = 0; i < this->waypointsIds.size(); i++) {
      if (this->waypointsIds[i].z < this->currentEntry.z) {
        j = i;
      }
    }
    if (j != -1) {
      // Adjust if it is possible counter to get next entry lane point
      if (j < this->waypointsIds.size()-1 ) j++;
      this->waypointsIds.erase(this->waypointsIds.begin(),
        this->waypointsIds.begin() + j);
      this->waypointsPositions.erase(this->waypointsPositions.begin(),
        this->waypointsPositions.begin() + j);
    }

    // Get the next entry position
    math::Vector3 nextEntryPosition = this->waypointsPositions[0];

    // Look for closest lanePoints and discard the lanePoints
    // that don't interest us. Then store lanePoints positions
    bool copy = false;
    for (int i = 0; i < _msg->lane_points_positions().size(); i++) {
      if (!copy) {
        double distance = sqrt(
          pow(_msg->lane_points_positions(i).x() - nextEntryPosition.x, 2) +
          pow(_msg->lane_points_positions(i).y() - nextEntryPosition.y, 2));
        if (distance < 0.1)
          copy = true;
      }
      if (copy) {
        const math::Vector3 new_position(
            _msg->lane_points_positions(i).x(),
            _msg->lane_points_positions(i).y(),
            _msg->lane_points_positions(i).z());
        this->pathLanePoints.push_back(new_position);
      }
    }

    // Store entries positions and ids don't save the new selected
    // entry and the previous entry used
    for (int i = 0; i < _msg->next_entries_positions().size(); i++) {
      if (_msg->exit_ids(i).z() < this->currentEntry.z) {
        continue;
      }
      // Save next possible entries positions
      const math::Vector3 new_possible_entry_position(
          _msg->next_entries_positions(i).x(),
          _msg->next_entries_positions(i).y(),
          _msg->next_entries_positions(i).z());
      this->entriesPositions.push_back(new_possible_entry_position);
      // Save next possible entries ids
      const math::Vector3 new_possible_entry_id(
          _msg->next_entries_ids(i).x(),
          _msg->next_entries_ids(i).y(),
          _msg->next_entries_ids(i).z());
      this->entriesIds.push_back(new_possible_entry_id);
      // Save next exit ids
      const math::Vector3 exit_id(
          _msg->exit_ids(i).x(),
          _msg->exit_ids(i).y(),
          _msg->exit_ids(i).z());
      this->exitIds.push_back(exit_id);
    }
    // Set blocking flag as false
    this->waitResponse = false;
  }
  else
  {
    gzwarn << "[road_driver] Received empty path." << std::endl;
  }

  this->mutex.unlock();
}

/////////////////////////////////////////////////
void RoadDriver::DefineNextEntry()
{
  math::Pose position = model->GetWorldPose();

  if (this->waitResponse) {
    return;
  }
  // Find if we are close to an entry
  if (this->pathLanePoints.size()) {
    unsigned int i = 0;
    while (i < this->exitIds.size()) {
      // Look for i exit position
      double exitPositionX = 0;
      double exitPositionY = 0;
      for (uint j = 0; j < this->waypointsIds.size(); j++) {
        // Look for the associated waypoint to the exit
        uint indexDifference = this->exitIds[i].z - this->waypointsIds[j].z;
        if (!indexDifference) {
          exitPositionX = this->waypointsPositions[j].x;
          exitPositionY = this->waypointsPositions[j].y;
          break;
        }
      }
      // Check if we are close to the exit point
      double distanceToExit = sqrt(
        pow(exitPositionX - position.pos.x, 2) +
        pow(exitPositionY - position.pos.y, 2));
      // TODO: make distance criteria  a parameter
      if (distanceToExit < 2) {
        // Check that is not the only available entry
        if (this->exitIds.size() > 1) {
          uint seed = static_cast<uint>(time(0));
          // Randomly select if took that entry or not
          if (rand_r(&seed) % 100 < 50) {
            // Request the lane points for the new entry
            if (this->entriesIds[i] != this->currentEntry) {
              RequestWaypoints(this->entriesIds[i]);
            }
            // Stop searching next entries, we already selected the last one
            break;
          }
          else
          {
            // If we don't choose that entry delete it and the next exit
            this->entriesIds.erase(this->entriesIds.begin() + i);
            this->exitIds.erase(this->exitIds.begin() + i);
            this->entriesPositions.erase(this->entriesPositions.begin() + i);
            continue;
          }
        }
        // If we are close to an entry, and it is the only one, take it
        else if (this->exitIds.size() == 1)
        {
          RequestWaypoints(this->entriesIds[i]);
          break;
        }
        else
        {
          gzmsg << "[road_driver] No exits available." << std::endl;
          break;
        }
      }
      // If it is too far away from i exit continue
      else
        i++;
    }
  }
  // If there is no lane points
  else
  {
    if (this->exitIds.size() >= 1) {
      RequestWaypoints(this->entriesIds[0]);
    }
    else if (this->exitIds.size() == 0)
    {
      // If something happened restart from the beginning
      gzwarn << "[road_driver] " << "Something happened. " <<
        "We do not have any lane points neither exitIds, " <<
        "so we are going to jump."
        << std::endl;
      RequestWaypoints(this->initialLanePointID);
      this->firstLanePoint = true;
    }
  }
}

/////////////////////////////////////////////////
math::Pose RoadDriver::DefineNextLanePointPosition()
{
  math::Pose position = model->GetWorldPose();
  math::Pose nextPosition = model->GetWorldPose();
  common::Time currentTime  = this->world->GetSimTime();

  // If there is lanePoints available
  if (this->pathLanePoints.size() > 0) {
    // If this is the first lanePoint after a jump
    if (this->firstLanePoint) {
      gzmsg << "[road_driver] Setting first lanePoint as position."
        << std::endl;
      math::Pose initialPosition(
          this->pathLanePoints[0].x,
          this->pathLanePoints[0].y,
          this->pathLanePoints[0].z, 0, 0, 0);
      this->model->SetWorldPose(initialPosition);
      firstLanePoint = false;
    }

    // update next goal
    nextPosition = math::Pose(this->pathLanePoints[0].x,
        this->pathLanePoints[0].y, 1, 0, 0, 0);

    // If it has arrived, delete current lanePoint
    double distance = sqrt(
      pow(nextPosition.pos.x - position.pos.x, 2) +
      pow(nextPosition.pos.y - position.pos.y, 2));
    if (distance < this->minimalDistance) {
      this->pathLanePoints.erase(this->pathLanePoints.begin());
    }
  }
  return nextPosition;
}

/////////////////////////////////////////////////
void RoadDriver::OnUpdate(const common::UpdateInfo& _info)
{
  this->mutex.lock();
  math::Pose position = model->GetWorldPose();
  common::Time currentTime  = this->world->GetSimTime();

  // Leave some time to Gazebo to being set properly
  if ((currentTime - this->startTime).Double() > this->initialWaitTime) {
    DefineNextEntry();
    // Continue moving through the lanePoints
    math::Pose nextPosition = DefineNextLanePointPosition();
    Move(nextPosition);
  }
  this->mutex.unlock();
}

/////////////////////////////////////////////////
void RoadDriver::RequestWaypoints(math::Vector3 waypointID)
{
  if (this->waitResponse)
    return;

  this->waitResponse = true;
  // Record previus used entry
  this->lastEntry = this->currentEntry;
  this->currentEntry = waypointID;

  // Create message with the request
  rndf_gazebo_plugin_msgs::msgs::WaypointsRequest waypointsRequestMsg;
  waypointsRequestMsg.set_client_name(this->model_name);
  waypointsRequestMsg.mutable_initial_waypoint()->set_x(waypointID.x);
  waypointsRequestMsg.mutable_initial_waypoint()->set_y(waypointID.y);
  waypointsRequestMsg.mutable_initial_waypoint()->set_z(waypointID.z);

  // Publish request
  this->waypointsRequestPub->WaitForConnection();
  this->waypointsRequestPub->Publish(waypointsRequestMsg);
}

/////////////////////////////////////////////////
void RoadDriver::Move(math::Pose positionTarget)
{
  common::Time currentTime  = this->world->GetSimTime();
  math::Pose position = model->GetWorldPose();
  common::Time stepTime = currentTime - this->lastTime;
  math::Pose positionError;

  // calculate the error between the current position and the target
  positionError.pos.x = position.pos.x - positionTarget.pos.x;
  positionError.pos.y = position.pos.y - positionTarget.pos.y;
  positionError.pos.z = position.pos.z - positionTarget.pos.z;

  // Calculate yaw error
  double yawError = atan2(positionError.pos.y,
      positionError.pos.x) - position.rot.GetYaw();
  if (yawError > M_PI)
    yawError -= 2*M_PI;
  else if (yawError < -M_PI)
    yawError += 2*M_PI;

  double rollEffort = this->axisRollPID.Update(
      position.rot.GetRoll(), stepTime);
  double pitchEffort = this->axisPitchPID.Update(
      position.rot.GetPitch(), stepTime);
  double yawEffort = this->axisYawZPID.Update(
      yawError, stepTime);
  math::Vector3 angular_vel(rollEffort, pitchEffort, yawEffort);
  this->model->SetAngularVel(angular_vel);

  // Apply velocities
  double velX = this->linearVelocity * cos(position.rot.GetYaw());
  double velY = this->linearVelocity * sin(position.rot.GetYaw());
  double velZ = this->axisZPID.Update(positionError.pos.z, stepTime);

  math::Vector3 linear_vel(velX, velY, velZ);
  this->model->SetLinearVel(linear_vel);
}

/////////////////////////////////////////////////
void RoadDriver::Reset()
{
  this->lastTime = this->world->GetSimTime();
  this->startTime = this->lastTime;
  this->firstLanePoint = false;
  this->waitResponse = false;
  this->pathLanePoints.clear();
  this->entriesIds.clear();
  this->entriesPositions.clear();
  this->waypointsIds.clear();
  this->waypointsPositions.clear();
  this->exitIds.clear();
}

}
